using Game.Ecs;
using Game.UI;
using UnityEngine;
using Zenject;

namespace Game
{
	public class GameInstaller : MonoInstaller
	{
		[SerializeField] private PlayerInputManager _playerInputManager;
		[SerializeField] private PlayerView _playerViewPrefab;
		[SerializeField] private EnemyView _enemyViewPrefab;
		[SerializeField] private AstarPath _astarPath;
		[SerializeField] private DevTools _devTools;
		[SerializeField] private Canvas _ui;
		[SerializeField] private EcsManager _ecsManager;
		[SerializeField] private PlayerHealthUI _playerHealthUI;

		public override void InstallBindings()
		{
			Container.BindInstance(_playerInputManager);
			Container.BindInstance(_playerViewPrefab);
			Container.BindInstance(_enemyViewPrefab);
			Container.BindInstance(_astarPath);
			Container.BindInstance(_devTools);
			Container.BindInstance(_ecsManager);
			Container.BindInstance(_playerHealthUI);
			Container.BindInstance(_ui).WithId(CanvasType.UI);
			Container.Bind<EcsSystemsFactory>().AsSingle();
			Container.Bind<IWindows>().To<Windows>().AsSingle();
			Container.Bind<Configs>().AsSingle().OnInstantiated((InjectContext _, Configs configs) => configs.Init());
			Container.Bind<AttackTypesKeeper>().AsSingle();
			Container.Bind<PrefabsInjectFactory>().AsSingle();
			Container.Bind<GridController>().AsSingle().OnInstantiated((InjectContext _, GridController gridController) => gridController.Init());
			Container.Bind<BuildingFactory>().AsSingle();
		}
	}
}
