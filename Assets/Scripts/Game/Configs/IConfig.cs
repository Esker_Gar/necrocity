﻿using System;

namespace Game
{
	public interface IConfig
	{
		Type GetComponentType();
	}
}