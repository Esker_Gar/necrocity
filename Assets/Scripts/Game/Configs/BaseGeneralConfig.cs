﻿using UnityEngine;

namespace Game
{
	public abstract class BaseGeneralConfig<T> : BaseConfig<T>, IGeneralConfig<T>
	where T : struct
	{
		[SerializeField] private T _config;

		public T Config => _config;
	}
}