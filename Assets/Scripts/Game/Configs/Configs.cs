﻿using System;
using System.Collections.Generic;
using System.Linq;
using Framework;
using JetBrains.Annotations;
using UnityEngine;

namespace Game
{
	[UsedImplicitly]
	public class Configs
	{
		private const string ConfigsPath = "Configs";
		private readonly Dictionary<Type, IConfig> _configByType = new();

		public void Init()
		{
			var baseConfigType = typeof(BaseConfig<>);
			var allScriptableObjects = Resources.LoadAll<ScriptableObject>(ConfigsPath);

			foreach (var scriptableObject in allScriptableObjects)
			{
				if (scriptableObject.GetType().IsGenericSubclassOfRecursive(baseConfigType))
				{
					var iConfig = (IConfig)scriptableObject;
					_configByType.Add(iConfig.GetComponentType(), iConfig);
				}
			}
		}

		public bool TryGetComponent<T>(int id, out T component)
			where T : struct
		{
			if (_configByType.TryGetValue(typeof(T), out var config) &&
			    config is IComponentConfig<T> componentConfig)
			{
				componentConfig.TryGetComponent(id, out component);
				
				return true;
			}

			component = default;

			return false;
		}

		public T GetConfig<T>()
		where T : struct
		{
			if (_configByType.TryGetValue(typeof(T), out var config) &&
			    config is IGeneralConfig<T> generalConfig)
            {
                return generalConfig.Config;
            }

			return default;
		}

		public IReadOnlyList<T> GetAllComponents<T>()
		{
			if (_configByType.TryGetValue(typeof(T), out var config) &&
			    config is IComponentConfig<T> componentConfig)
			{
				return componentConfig.AllComponents.ToList();
			}

			return default;
		}
	}
}