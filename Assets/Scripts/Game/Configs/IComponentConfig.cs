﻿using System.Collections.Generic;

namespace Game
{
	public interface IComponentConfig<T> : IConfig
	{
		IEnumerable<T> AllComponents {get;}

		bool TryGetComponent(int id, out T component);
	}
}