﻿using System;
using UnityEngine;

namespace Game
{
	[Serializable]
	public struct UnitParameters
	{
		public int Id;
		public float Speed;
		public float Health;
		public int AttackTypeId;
	}

	[CreateAssetMenu(fileName = "UnitParameters", menuName = "Configs/Model/UnitParameters")]
	public class UnitConfig : BaseComponentConfig<UnitParameters>
	{

		protected override void Serialize()
		{}

		protected override void Deserialize()
		{
			_dictionary.Clear();

			foreach (var parameters in _array)
			{
				if (_dictionary.TryAdd(parameters.Id, parameters) == false)
				{
					Debug.LogError("ConfigUnitParameters have non unique ids");
				}
			}
		}
	}
}