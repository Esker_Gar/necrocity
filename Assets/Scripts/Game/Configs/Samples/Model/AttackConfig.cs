﻿using System;
using UnityEngine;

namespace Game
{
	[Serializable]
	public struct AttackParameters
	{
		public int UnitId;
		public float AttackDamage;
		public float AttackRange;
		public float AttackAngle;
		public float Cooldown;
		public int MaxTargets;
	}
	
	[CreateAssetMenu(fileName = "MeleeAttackParameters", menuName = "Configs/Model/MeleeAttackParameters")]
	public class AttackConfig : BaseComponentConfig<AttackParameters>
	{

		protected override void Serialize()
		{
		}

		protected override void Deserialize()
		{
			_dictionary.Clear();
            
			foreach (var parameters in _array)
			{
				if (_dictionary.TryAdd(parameters.UnitId, parameters) == false)
				{
					Debug.LogError("ConfigUnitParameters have non unique ids");
				}
			}
		}
	}
}