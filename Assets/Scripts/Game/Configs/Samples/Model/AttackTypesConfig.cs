﻿using System;
using UnityEngine;

namespace Game
{
	[Serializable]
	public struct AttackTypesParameters
	{
		public int Id;
		public string Name;
	}
	
	[CreateAssetMenu(fileName = "AttackTypesParameters", menuName = "Configs/Model/AttackTypesParameters")]
	public class AttackTypesConfig : BaseComponentConfig<AttackTypesParameters>
	{

		protected override void Serialize()
		{
			
		}

		protected override void Deserialize()
		{
			_dictionary.Clear();

			foreach (var parameters in _array)
			{
				if (_dictionary.TryAdd(parameters.Id, parameters) == false)
				{
					Debug.LogError("ConfigUnitParameters have non unique ids");
				}
			}
		}
	}
}