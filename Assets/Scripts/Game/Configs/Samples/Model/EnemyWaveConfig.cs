﻿using System;
using UnityEngine;

namespace Game
{
	[Serializable]
	public struct EnemyWaveParameters
	{
		public int Id;
		public int[] PossibleEnemies;
		public int EnemyAmount;
	}
	
	[CreateAssetMenu(fileName = "EnemyWaveParameters", menuName = "Configs/Model/EnemyWaveParameters")]
	public class EnemyWaveConfig : BaseComponentConfig<EnemyWaveParameters>
	{
		protected override void Serialize()
		{
		}

		protected override void Deserialize()
		{
			_dictionary.Clear();

			foreach (var parameters in _array)
			{
				if (_dictionary.TryAdd(parameters.Id, parameters) == false)
				{
					Debug.LogError("ConfigUnitParameters have non unique ids");
				}
			}
		}
	}
}