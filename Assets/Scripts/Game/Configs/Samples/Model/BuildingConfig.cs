﻿using System;
using UnityEngine;

namespace Game
{
	[Serializable]
	public struct BuildingParameters
	{
		public int Id;
		public float InteractRadius;
		public Vector3 Position;
		public Vector3 Size;
		public BuildingType BuildingType;
		public int[] PossibleBuildingIds;
	}
	
	[CreateAssetMenu(fileName = "BuildingParameters", menuName = "Configs/Model/BuildingParameters")]
	public class BuildingConfig : BaseComponentConfig<BuildingParameters>
	{
		protected override void Serialize()
		{
		}

		protected override void Deserialize()
		{
			_dictionary.Clear();
			
			foreach (var parameters in _array)
			{
				if (_dictionary.TryAdd(parameters.Id, parameters) == false)
				{
					Debug.LogError("ConfigUnitParameters have non unique ids");
				}
			}
		}
	}
}