﻿using System;
using UnityEngine;

namespace Game
{
    [Serializable]
    public struct BuildingUIParameters
    {
        public int Id;
        public Sprite Sprite;
        public string Descripton;
    }
    
    [CreateAssetMenu(fileName = "BuildingUIParameters", menuName = "Configs/UI/BuildingUIParameters")]
    public class BuildingUIConfig : BaseComponentConfig<BuildingUIParameters>
    {
        protected override void Serialize()
        {
        }

        protected override void Deserialize()
        {
            _dictionary.Clear();

            foreach (var baseComponent in _array)
            {
                if (_dictionary.TryAdd(baseComponent.Id, baseComponent) == false)
                {
                    Debug.LogError("ConfigUnitParameters have non unique ids");
                }
            }
        }
    }
}