﻿using System;
using UnityEngine;

namespace Game
{
	[Serializable]
	public struct GameConfigurations
	{
		public int PlayerId;
		public float GameDayInSeconds;
		public float StartWaveTime;
		public float CheckToInteractTime;
	}
	
	[CreateAssetMenu(fileName = "GameConfigs", menuName = "Configs/General/GameConfigs")]
	public class GameConfigs : BaseGeneralConfig<GameConfigurations>
	{
		
	}
}