﻿using System;
using UnityEngine;

namespace Game
{
	[Serializable]
	public struct EnemyConfigurations
	{
		public float EnemySpawnInnerRadius;
		public float EnemySpawnOuterRadius;
		public float UpdatePathToTargetTime;
		public float NextWaypointDistance;
	}
	
	[CreateAssetMenu(fileName = "EnemyConfigs", menuName = "Configs/General/EnemyConfigs")]
	public class EnemyConfigs : BaseGeneralConfig<EnemyConfigurations>
	{
		
	}
}