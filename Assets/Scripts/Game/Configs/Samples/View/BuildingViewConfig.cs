﻿using System;
using UnityEngine;

namespace Game
{
	[Serializable]
	public struct BuildingViewParameters
	{
		public int Id;
		public BaseBuildingView Prefab;
		public Vector3 Size;
	}
	
	[CreateAssetMenu(fileName = "BuildingViewParameters", menuName = "Configs/View/BuildingViewParameters")]
	public class BuildingViewConfig : BaseComponentConfig<BuildingViewParameters>
	{
		protected override void Serialize()
		{
		}

		protected override void Deserialize()
		{
			_dictionary.Clear();

			foreach (var baseComponent in _array)
			{
				if (_dictionary.TryAdd(baseComponent.Id, baseComponent) == false)
				{
					Debug.LogError("ConfigUnitParameters have non unique ids");
				}
			}
		}
	}
}