﻿using System.Collections.Generic;
using UnityEngine;

namespace Game
{
	public abstract class BaseComponentConfig<T> : BaseConfig<T>, IComponentConfig<T>, ISerializationCallbackReceiver
	where T :struct
	{
		[SerializeField] protected T[] _array;
		
		public IEnumerable<T> AllComponents => _dictionary.Values;

		protected readonly Dictionary<int, T> _dictionary = new();
		
		protected abstract void Serialize();
		protected abstract void Deserialize();

		public void OnBeforeSerialize()
		{
			Serialize();
		}

		public void OnAfterDeserialize()
		{
			Deserialize();
		}

		public bool TryGetComponent(int id, out T component)
		{
			if (_dictionary.TryGetValue(id, out component))
			{
				return true;
			}

			component = default;

			return false;
		}
	}
}