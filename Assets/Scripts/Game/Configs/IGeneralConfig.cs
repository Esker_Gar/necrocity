﻿namespace Game
{
	public interface IGeneralConfig<out T> : IConfig
	where T : struct
	{
		T Config {get;}
	}
}