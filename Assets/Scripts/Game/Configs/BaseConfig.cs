﻿using System;
using UnityEngine;

namespace Game
{
	public abstract class BaseConfig<T> : ScriptableObject, IConfig
	where T : struct
	{
		public Type GetComponentType()
		{
			return typeof(T);
		}
	}
}