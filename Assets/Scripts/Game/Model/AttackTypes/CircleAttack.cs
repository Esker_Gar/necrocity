﻿using System.Collections.Generic;
using Framework;
using Leopotam.EcsLite;
using UnityEngine;

namespace Game
{
    public class CircleAttack : IAttackType
    {
		private readonly GridController _gridController;
		private readonly HashSet<int> _targetIds = new();

		private AttackComponent _attackComponent;
		private Vector3 _pawnPosition;
		private int _entityId;

		public CircleAttack(GridController gridController)
		{
			_gridController = gridController;
		}
		
		public IEnumerable<int> GetTargets(int entityId, EcsWorld world)
		{
			_targetIds.Clear();
			_entityId = entityId;
			_attackComponent = world.GetComponent<AttackComponent>(entityId);
			var positionComponent = world.GetComponent<PositionComponent>(entityId);
			var teamComponent = world.GetComponent<TeamComponent>(entityId);
			var range = _attackComponent.AttackRange;
			_pawnPosition = positionComponent.Position;
			
			var bounds = new Bounds(_pawnPosition, Vector3.one * range);
			var nodes = _gridController.GetNodesInRegion(bounds);
			var range2 = range * range;

			foreach (var node in nodes)
			{
				var targets = _gridController.GetTargets(node);

				AddFitTargets(targets, world, teamComponent.TeamType, range2);
			}

			return _targetIds;
		}

		private void AddFitTargets(HashSet<int> targets, EcsWorld world, TeamType teamType, float range2)
		{
			if (targets.Count <= 0)
			{
				return;
			}
			
			foreach (var id in targets)
			{
				if (id == _entityId)
				{
					continue;
				}
				
				var enemyPositionComponent = world.GetComponent<PositionComponent>(id);
				var teamComponent = world.GetComponent<TeamComponent>(id);

				if (teamComponent.TeamType == teamType)
				{
					continue;
				}
				
				var sqrMagnitude = (enemyPositionComponent.Position - _pawnPosition).sqrMagnitude;
				
				if (sqrMagnitude <= range2)
				{
					_targetIds.Add(id);
				}
			}
		}
    }
}