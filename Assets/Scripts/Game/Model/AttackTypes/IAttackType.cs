﻿using System.Collections.Generic;
using Leopotam.EcsLite;

namespace Game
{
	public interface IAttackType
	{
		IEnumerable<int> GetTargets(int entityId, EcsWorld world);
	}
}