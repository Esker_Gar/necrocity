﻿using System.Collections.Generic;
using JetBrains.Annotations;
using Zenject;

namespace Game
{
	public enum AttackType
	{
		Cone,
		Circle
	}

	[UsedImplicitly]
	public class AttackTypesKeeper
	{
		public IReadOnlyDictionary<AttackType, IAttackType> AttackTypes => _attackTypes;

		private readonly Dictionary<AttackType, IAttackType> _attackTypes;

		[Inject]
		public AttackTypesKeeper(GridController gridController)
		{
			_attackTypes = new Dictionary<AttackType, IAttackType>
			{
				{AttackType.Cone, new ConeAttack(gridController)},
				{AttackType.Circle, new CircleAttack(gridController)}
			};

		}
	}
}