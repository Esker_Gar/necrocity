﻿using System.Collections.Generic;
using Framework;
using Leopotam.EcsLite;
using UnityEngine;

namespace Game
{
	public class ConeAttack : IAttackType
	{
		private readonly GridController _gridController;
		private readonly HashSet<int> _targetIds = new();

		private AttackComponent _attackComponent;
		private Vector3 _pawnPosition;
		private int _entityId;

		public ConeAttack(GridController gridController)
		{
			_gridController = gridController;
		}
		
		public IEnumerable<int> GetTargets(int entityId, EcsWorld world)
		{
			_targetIds.Clear();
			_entityId = entityId;
			_attackComponent = world.GetComponent<AttackComponent>(entityId);
			var positionComponent = world.GetComponent<PositionComponent>(entityId);
			var rotationComponent = world.GetComponent<RotationComponent>(entityId);
			var teamComponent = world.GetComponent<TeamComponent>(entityId);
			var range = _attackComponent.AttackRange;
			_pawnPosition = positionComponent.Position;
			
			var baseDirection = Vector3.forward;
			var lookDirection = rotationComponent.Rotation * baseDirection;

			var attackCenter = _pawnPosition + lookDirection * range * .5f;
			var bounds = new Bounds(attackCenter, Vector3.one * range);

			var nodes = _gridController.GetNodesInRegion(bounds);

			foreach (var node in nodes)
			{
				var targets = _gridController.GetTargets(node);

				AddFitTargets(targets, world, lookDirection, teamComponent.TeamType);
			}

			return _targetIds;
		}

		private void AddFitTargets(HashSet<int> targets, EcsWorld world, Vector3 lookDirection, TeamType teamType)
		{
			if (targets.Count <= 0)
			{
				return;
			}
			
			var halfAngle = _attackComponent.AttackAngle * .5f;
			
			foreach (var id in targets)
			{
				if (id == _entityId)
				{
					continue;
				}
				
				var enemyPositionComponent = world.GetComponent<PositionComponent>(id);
				var teamComponent = world.GetComponent<TeamComponent>(id);

				if (teamComponent.TeamType == teamType)
				{
					continue;
				}
				
				var directionToNode = (enemyPositionComponent.Position - _pawnPosition).normalized;
				var angleBetweenVectors = Vector3.Angle(lookDirection, directionToNode);

				if (angleBetweenVectors <= halfAngle)
				{
					_targetIds.Add(id);
				}
			}
		}
	}
}