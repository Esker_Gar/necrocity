﻿using System;
using Framework;
using JetBrains.Annotations;

namespace Game
{
    [Serializable]
    [UsedImplicitly]
    public class TowerSpotBuilding : BaseBuilding
    {
        protected override void HandleInit(BuildingParameters buildingParameters)
        {
            EntityId = World.NewEntity();
				
            if (this is IInteractable interactable)
            {
                ref var interactComponent = ref World.AddComponent<InteractComponent>(EntityId);
                ref var positionComponent = ref World.AddComponent<PositionComponent>(EntityId);
                ref var idComponent = ref World.AddComponent<IdComponent>(EntityId);
                ref var buildingComponent = ref World.AddComponent<BuildingComponent>(EntityId);

                World.AddComponent<DestroyComponent>(EntityId);
					
                interactComponent.Interactable = interactable;
                interactComponent.InteractRadiusPow2 = buildingParameters.InteractRadius * buildingParameters.InteractRadius;
                positionComponent.Position = buildingParameters.Position;
                idComponent.Id = EntityId;
                buildingComponent.BaseBuilding = this;
                buildingComponent.BuildingId = buildingParameters.Id;
            }
        }

        protected override void HandleInteract()
        {
            
        }
    }
}