﻿using System;
using System.Linq;
using Framework;
using JetBrains.Annotations;
using Leopotam.EcsLite;
using UnityEngine;
using Zenject;

namespace Game
{
	[Serializable]
	[UsedImplicitly]
	public class GatesBuilding : BaseBuilding, IPassabilityModifier
	{
		[Inject] private readonly GridController _gridController;

		public event Action<bool> OnOpenChanged;
		public bool IsOpen
		{
			get => _isOpen;
			set
			{
				_isOpen = value;
				
				OnOpenChanged?.Invoke(_isOpen);
			}
		}

	
		private bool _isOpen;

		protected override void HandleInit(BuildingParameters buildingParameters)
		{
			IsOpen = true;
			
			EntityId = World.NewEntity();
				
			if (this is IInteractable interactable)
			{
				ref var interactComponent = ref World.AddComponent<InteractComponent>(EntityId);
				ref var positionComponent = ref World.AddComponent<PositionComponent>(EntityId);
				ref var idComponent = ref World.AddComponent<IdComponent>(EntityId);
				ref var buildingComponent = ref World.AddComponent<BuildingComponent>(EntityId);

				World.AddComponent<DestroyComponent>(EntityId);
					
				interactComponent.Interactable = interactable;
				interactComponent.InteractRadiusPow2 = buildingParameters.InteractRadius * buildingParameters.InteractRadius;
				positionComponent.Position = buildingParameters.Position;
				idComponent.Id = EntityId;
				buildingComponent.BaseBuilding = this;
				buildingComponent.BuildingId = buildingParameters.Id;

				if (this is IPassabilityModifier modifier)
				{
					var bounds = new Bounds(buildingParameters.Position, buildingParameters.Size * 1.5f);
					var nodes = _gridController.GetNodesInRegion(bounds).ToArray();

					_gridController.AddPassabilityModifier(modifier, nodes);
					ref var gateInteractEventComponent = ref World.AddComponent<GateInteractEventComponent>(EntityId);
					gateInteractEventComponent.PassabilityModifier = modifier;
				}
			}
		}

		protected override void HandleInteract()
		{
			ref var gateInteractEventComponent = ref World.AddComponent<GateInteractEventComponent>(EntityId);
			gateInteractEventComponent.PassabilityModifier = this;
		}
	}
}