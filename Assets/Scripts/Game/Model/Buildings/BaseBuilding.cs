﻿using System;
using Leopotam.EcsLite;

namespace Game
{
	[Serializable]
	public abstract class BaseBuilding : IInteractable
	{
		public event Action OnInteract;

		public int BuildingId { get; private set; }
		public int EntityId { get; protected set; }
		
		protected EcsWorld World;

		protected abstract void HandleInit(BuildingParameters buildingParameters);
		protected abstract void HandleInteract();

		public void Init(int buildingId, EcsWorld ecsWorld, BuildingParameters buildingParameters)
		{
			BuildingId = buildingId;
			World = ecsWorld;
			
			HandleInit(buildingParameters);
		}

		public void Interact()
		{
			HandleInteract();
			OnInteract?.Invoke();
		}
	}
}