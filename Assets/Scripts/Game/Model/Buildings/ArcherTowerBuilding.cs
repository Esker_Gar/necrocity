﻿using Framework;
using Zenject;

namespace Game
{
    public class ArcherTowerBuilding : BaseBuilding
    {
        [Inject] private readonly Configs _configs;
        
        protected override void HandleInit(BuildingParameters buildingParameters)
        {
            EntityId = World.NewEntity();
            
            if (this is IInteractable interactable)
            {
                ref var interactComponent = ref World.AddComponent<InteractComponent>(EntityId);
                ref var positionComponent = ref World.AddComponent<PositionComponent>(EntityId);
                ref var idComponent = ref World.AddComponent<IdComponent>(EntityId);
                ref var buildingComponent = ref World.AddComponent<BuildingComponent>(EntityId);
                ref var attackComponent = ref World.AddComponent<AttackComponent>(EntityId);
                ref var teamComponent = ref World.AddComponent<TeamComponent>(EntityId);

                World.AddComponent<DestroyComponent>(EntityId);
					
                interactComponent.Interactable = interactable;
                interactComponent.InteractRadiusPow2 = buildingParameters.InteractRadius * buildingParameters.InteractRadius;
                positionComponent.Position = buildingParameters.Position;
                idComponent.Id = EntityId;
                buildingComponent.BaseBuilding = this;
                buildingComponent.BuildingId = buildingParameters.Id;

                teamComponent.TeamType = TeamType.Player;
                
                if(_configs.TryGetComponent(idComponent.Id, out AttackParameters attackParameters))
                {
                    attackComponent.Damage = attackParameters.AttackDamage;
                    attackComponent.AttackRange = attackParameters.AttackRange;
                    attackComponent.MaxTargets = attackParameters.MaxTargets;
                }
            }
        }

        protected override void HandleInteract()
        {
        }
    }
}