﻿using Framework;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Game.UI
{
    public class TowersWindowArgs : IWindowArgs
    {
        public readonly BaseBuilding BaseBuilding;

        public TowersWindowArgs(BaseBuilding baseBuilding)
        {
            BaseBuilding = baseBuilding;
        }
    }

    public class TowersWindowUI : Window
    {
        [SerializeField] private TowerUI _towerPrefab;
        [SerializeField] private Transform _buildingsContainer;
        [SerializeField] private Button _buildButton;
        [SerializeField] private Button _closeButton;
        [SerializeField] private TMP_Text _description;

        [Inject] private Configs _configs;
        [Inject] private EcsManager _ecsManager;

        private TowerUI _currentTowerUI;
        private int _entityId;

        public override void Init(IWindowArgs windowArgs)
        {
            if (windowArgs is TowersWindowArgs towersWindowArgs)
            {
                _entityId = towersWindowArgs.BaseBuilding.EntityId;
                CreateBuildings(towersWindowArgs.BaseBuilding.BuildingId);
            }
            _closeButton.onClick.AddListener(Close);
            _buildButton.onClick.AddListener(BuildClick);
            
            _buildButton.SetActive(false);
        }

        private void CreateBuildings(int id)
        {
            _buildingsContainer.DestroyChildren();

            if (_configs.TryGetComponent(id, out BuildingParameters buildingParameters))
            {
                foreach (var buildingId in buildingParameters.PossibleBuildingIds)
                {
                    if (_configs.TryGetComponent(buildingId, out BuildingUIParameters buildingUIParameters))
                    {
                        TowerUI towerUI = Instantiate(_towerPrefab, _buildingsContainer);

                        towerUI.Set(buildingUIParameters);
                        towerUI.Button.onClick.AddListener(() => UpdateTowerUI(towerUI));
                    }
                }
            }
        }

        private void UpdateTowerUI(TowerUI towerUI)
        {
            _currentTowerUI = towerUI;

            if (_configs.TryGetComponent(_currentTowerUI.BuildingId, out BuildingUIParameters buildingUIParameters))
            {
                _description.SetText(buildingUIParameters.Descripton);
            }
            
            _buildButton.SetActive(true);
        }

        private void BuildClick()
        {
            ref BuildEventComponent buildEventComponent = ref _ecsManager.World.AddComponent<BuildEventComponent>(_ecsManager.GameEntityId);
            buildEventComponent.BuildingId = _currentTowerUI.BuildingId;

            _ecsManager.World.AddComponent<DestroyEventComponent>(_entityId);
            
            Close();
        }
    }
}