﻿using UnityEngine;
using UnityEngine.UI;

namespace Game.UI
{
    public class TowerUI : MonoBehaviour
    {
        [SerializeField] private Image _image;
        [SerializeField] private Button _button;
        
        public int BuildingId { get; private set; }
        public Button Button => _button;

        public void Set(BuildingUIParameters buildingUIParameters)
        {
            BuildingId = buildingUIParameters.Id;
            _image.sprite = buildingUIParameters.Sprite;
        }
    }
}