﻿namespace Game.UI
{
    public interface IHealthUI
    {
        void Set(float health);
        void SetMaxHealth(float maxHealth);
    }
}