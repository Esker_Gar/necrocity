﻿using UnityEngine;
using UnityEngine.UI;

namespace Game.UI
{
    public class PlayerHealthUI : MonoBehaviour, IHealthUI
    {
        [SerializeField] private Image _fill;

        private float _maxHealth;
        
        public void SetMaxHealth(float maxHealth)
        {
            _maxHealth = maxHealth;
        }
        
        public void Set(float health)
        {
            _fill.fillAmount = health / _maxHealth;
        }
    }
}