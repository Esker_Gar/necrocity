﻿using UnityEngine;

namespace Game
{
	public interface IMovable
	{
		Vector3 Move(Vector3 velocity);
	}
}