﻿using Game.Ecs;
using Game.Ecs.Systems;
using Game.Ecs.Systems.Model;
using Game.Ecs.Systems.View;
using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;
using UnityEngine;
using Zenject;

namespace Game
{
	public class EcsManager : MonoBehaviour
	{
		[Inject] private readonly EcsSystemsFactory _ecsSystemsFactory;
		
		public EcsWorld World { get; private set; }
		public int GameEntityId { get; set; }
		
		private EcsSystems _modelSystems;
		private EcsSystems _modelFixedSystems;
		private EcsSystems _viewSystems;
		private EcsSystems _viewLateUpdateSystems;
		private EcsSystems _deleteSystems;
		
		private void Start()
		{
			World = new EcsWorld();

			_modelSystems = new EcsSystems(World);
			_modelFixedSystems = new EcsSystems(World);
			_viewSystems = new EcsSystems(World);
			_viewLateUpdateSystems = new EcsSystems(World);
			_deleteSystems = new EcsSystems(World);

			_modelSystems
				.Add(_ecsSystemsFactory.Create<InputSystem>())
				.Add(_ecsSystemsFactory.Create<CreateGameEntitySystem>())
				.Add(_ecsSystemsFactory.Create<CreatePlayerEntitySystem>())
				.Add(_ecsSystemsFactory.Create<CreateBuildingsSystem>())
				.Add(_ecsSystemsFactory.Create<BehaviourTreeTickSystem>())
				.Add(_ecsSystemsFactory.Create<TimerSystem>())
				.Add(_ecsSystemsFactory.Create<SpawnEnemySystem>())
				.Add(_ecsSystemsFactory.Create<UpdatePathToTargetSystem>())
				.Add(_ecsSystemsFactory.Create<AiRotateSystem>())
				.Add(_ecsSystemsFactory.Create<FindTargetSystem>())
				.Add(_ecsSystemsFactory.Create<CheckInteractDistanceSystem>())
				.Add(_ecsSystemsFactory.Create<InteractSystem>())
				.Add(_ecsSystemsFactory.Create<TowerCheckAttackSystem>())
				.Add(_ecsSystemsFactory.Create<CheckAttackSystem>())
				.Add(_ecsSystemsFactory.Create<TakeDamageSystem>())
				.Add(_ecsSystemsFactory.Create<GateInteractSystem>())
				.Inject()
				.Init();

			_modelFixedSystems
				.Add(_ecsSystemsFactory.Create<AiUpdateDirectionSystem>())
				.Add(_ecsSystemsFactory.Create<CheckAiReachAttackDistanceSystem>())
				.Add(_ecsSystemsFactory.Create<MoveSystem>())
				.Add(_ecsSystemsFactory.Create<UpdateAiPositionSystem>())
				.Inject()
				.Init();

			_viewSystems
				.Add(_ecsSystemsFactory.Create<CreatePlayerViewSystem>())
				.Add(_ecsSystemsFactory.Create<CreateBuildingsViewSystem>())
				.Add(_ecsSystemsFactory.Create<SpawnEnemyViewSystem>())
				.Add(_ecsSystemsFactory.Create<SyncAiViewPositionSystem>())
				.Add(_ecsSystemsFactory.Create<SyncViewRotationSystem>())
				.Add(_ecsSystemsFactory.Create<SyncHealthViewSystem>())
				.Inject()
				.Init();	
			
			_viewLateUpdateSystems
				.Add(_ecsSystemsFactory.Create<SyncCameraTransformSystem>())
				.Inject()
				.Init();	
			
			_deleteSystems
				.Add(_ecsSystemsFactory.Create<DeadSystem>())
				.Add(_ecsSystemsFactory.Create<DestroySystem>())
				.Add(_ecsSystemsFactory.Create<DeadViewSystem>())
				.Add(_ecsSystemsFactory.Create<DestroyViewSystem>())
				.Add(_ecsSystemsFactory.Create<EventsDeleteSystem>())
				.Inject()
				.Init();
		}

		private void Update()
		{
			_modelSystems?.Run();
			_viewSystems?.Run();
			_deleteSystems?.Run();
		}

		private void FixedUpdate()
		{
			_modelFixedSystems?.Run();
		}

		private void LateUpdate()
		{
			_viewLateUpdateSystems?.Run();
		}

		private void OnDestroy()
		{
			_modelSystems?.Destroy();
			_modelFixedSystems?.Destroy();
			_viewSystems?.Destroy();
			_deleteSystems?.Destroy();
			_viewLateUpdateSystems?.Destroy();
			
			_modelSystems = null;
			_modelFixedSystems = null;
			_viewSystems = null;
			_deleteSystems = null;
			_viewLateUpdateSystems = null;
			
			World?.Destroy();
			World = null;
		}
	}
}