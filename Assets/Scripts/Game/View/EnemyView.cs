﻿using Pathfinding;
using UnityEngine;

namespace Game
{
	public class EnemyView : MonoBehaviour
	{
		[SerializeField] private Seeker _seeker;

		public Seeker Seeker => _seeker;
	}
}