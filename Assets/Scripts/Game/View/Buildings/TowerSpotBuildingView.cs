﻿using Game.UI;
using UnityEngine;
using Zenject;

namespace Game
{
    public class TowerSpotBuildingView : BuildingView<TowerSpotBuilding>
    {
        [SerializeField] private GameObject _visualGameObject;

        [Inject] private IWindows _windows;
        
        private TowerSpotBuilding _towerSpotBuilding;

        protected override void Init(TowerSpotBuilding building)
        {
            _towerSpotBuilding = building;
            _towerSpotBuilding.OnInteract += OnInteracted;
        }

        protected override void OnDestroyHandle()
        {
            _towerSpotBuilding.OnInteract -= OnInteracted;
        }

        private void OnInteracted()
        {
            _windows.Open<TowersWindowUI>(new TowersWindowArgs(_towerSpotBuilding));
        }
    }
}