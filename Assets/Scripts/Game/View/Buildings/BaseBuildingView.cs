﻿using UnityEngine;

namespace Game
{
	public abstract class BaseBuildingView : MonoBehaviour
	{
		public abstract void Init(BaseBuilding building);
	}
}