﻿using UnityEngine;

namespace Game
{
	public class GatesBuildingView : BuildingView<GatesBuilding>
	{
		[SerializeField] private GameObject _visualGameObject;

		private GatesBuilding _gatesBuilding;

		protected override void Init(GatesBuilding building)
		{
			_gatesBuilding = building;
			_gatesBuilding.OnOpenChanged += OnGatesInteract;
			_visualGameObject.SetActive(_gatesBuilding.IsOpen);
		}

		protected override void OnDestroyHandle()
		{
			_gatesBuilding.OnOpenChanged -= OnGatesInteract;
		}

		private void OnGatesInteract(bool isOpen)
		{
			_visualGameObject.SetActive(isOpen);
		}
	}
}