﻿using UnityEngine;

namespace Game
{
	public abstract class BuildingView<T> : BaseBuildingView
	where T : BaseBuilding
	{
		protected abstract void Init(T building);
		protected abstract void OnDestroyHandle();
		
		public override void Init(BaseBuilding building)
		{
			if (building is T typedBuilding)
			{
				Init(typedBuilding);
			}
			else
			{
				Debug.LogError($"Invalid building type for {GetType().Name}: {building.GetType().Name}");
			}
		}

		private void OnDestroy()
		{
			OnDestroyHandle();
		}
	}
}