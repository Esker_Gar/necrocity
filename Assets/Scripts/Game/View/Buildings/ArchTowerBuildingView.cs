﻿using Game.UI;
using Zenject;

namespace Game
{
    public class ArchTowerBuildingView : BuildingView<ArcherTowerBuilding>
    {
        [Inject] private IWindows _windows;
        
        private ArcherTowerBuilding _archerTowerBuilding;

        protected override void Init(ArcherTowerBuilding building)
        {
            _archerTowerBuilding = building;
            _archerTowerBuilding.OnInteract += OnInteracted;
        }

        protected override void OnDestroyHandle()
        {
            _archerTowerBuilding.OnInteract -= OnInteracted;
        }

        private void OnInteracted()
        {
            _windows.Open<TowersWindowUI>(new TowersWindowArgs(_archerTowerBuilding));
        }
    }
}