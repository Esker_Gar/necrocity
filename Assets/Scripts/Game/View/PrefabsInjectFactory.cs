﻿using JetBrains.Annotations;
using UnityEngine;
using Zenject;

namespace Game
{
    [UsedImplicitly]
    public class PrefabsInjectFactory
    {
        [Inject] private readonly DiContainer  _diContainer;
        
        public T Create<T>(T prefab)
        where T : MonoBehaviour
        {
            return _diContainer.InstantiatePrefabForComponent<T>(prefab);
        }
    }
}