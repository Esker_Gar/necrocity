﻿using UnityEngine;

namespace Game
{
	public class PlayerView : MonoBehaviour, IMovable
	{
		[SerializeField] private Rigidbody _rigidbody;
		
		public Vector3 Move(Vector3 velocity)
		{
			_rigidbody.velocity = velocity;
			
			return _rigidbody.position;
		}
	}
}