﻿using System;
using UnityEngine;

namespace Game
{
    public abstract class Window : MonoBehaviour
    {
        public event Action OnClosed;
        
        public abstract void Init(IWindowArgs args);
        public virtual void HandleClose() {}

        protected void Close()
        {
            HandleClose();
            OnClosed?.Invoke();
            Destroy(gameObject);
        }
    }
    
    public interface IWindowArgs
    {}
}