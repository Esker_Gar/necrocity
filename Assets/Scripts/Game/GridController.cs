﻿using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using Pathfinding;
using UnityEngine;
using Zenject;

namespace Game
{
	[UsedImplicitly]
	public class GridController
	{
		[Inject] private readonly AstarPath _astarPath;

		public IReadOnlyDictionary<IPassabilityModifier, GraphNode[]> PassabilityModifiers => _passabilityModifiers;
		
		private readonly Dictionary<GraphNode, HashSet<int>> _targetsByNodes = new();
		private readonly Dictionary<IPassabilityModifier, GraphNode[]> _passabilityModifiers = new();

		public void Init()
		{
			foreach (var graphNode in _astarPath.data.gridGraph.nodes)
			{
				_targetsByNodes[graphNode] = new HashSet<int>();
			}
		}

		public HashSet<int> GetTargets(GraphNode graphNode)
		{
			return _targetsByNodes[graphNode];
		}

		public IEnumerable<GraphNode> GetNodesInRegion(Bounds bounds)
		{
			return _astarPath.data.gridGraph.GetNodesInRegion(bounds);
		}

		public void RemoveEntityOnNode(GraphNode graphNode, int entityId)
		{
			_targetsByNodes[graphNode].Remove(entityId);
		}
		
		public void AddEntityOnNode(GraphNode graphNode, int entityId)
		{
			_targetsByNodes[graphNode].Add(entityId);
		}

		public GraphNode GetNode(Vector3 position)
		{
			return _astarPath.data.gridGraph.GetNearest(position).node;
		}

		public void AddPassabilityModifier(IPassabilityModifier modifier, IEnumerable<GraphNode> nodes)
		{
			_passabilityModifiers.Add(modifier, nodes.ToArray());
		}

		public void SetNodesWalkable(IPassabilityModifier modifier, bool isWalkable)
		{
			if (_passabilityModifiers.TryGetValue(modifier, out var nodes))
			{
				foreach (var node in nodes)
				{
					node.Walkable = isWalkable;
				}
                
				_astarPath.UpdateGraphs(nodes.GetBounds());
			}
		}
	}
}