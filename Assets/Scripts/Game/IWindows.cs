﻿namespace Game
{
    public interface IWindows
    {
        void Open<T>(IWindowArgs args) where T : Window;
    }
}