﻿using Pathfinding;
using UnityEngine;

namespace Game
{
    public static class GraphNodeExtensions
    {
        public static Bounds GetBounds(this GraphNode[] nodes)
        {
            if (nodes.Length == 0)
            {
                return default;
            }

            var min = (Vector3) nodes[0].position;
            var max = min;

            foreach (GraphNode node in nodes)
            {
                var pos = (Vector3)node.position;
                
                min = Vector3.Min(min, pos);
                max = Vector3.Max(max, pos);
            }

            var size = max - min;
            var center = (max + min) * .5f;

           return new Bounds(center, size);
        }
    }
}