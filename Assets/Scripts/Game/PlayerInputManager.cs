﻿using UnityEngine;

namespace Game
{
	public class PlayerInputManager : MonoBehaviour
	{
		private PlayerInput _playerInput;
		
		private void Awake()
		{
			_playerInput = new PlayerInput();
		}

		private void OnEnable()
		{
			_playerInput.Enable();
		}

		private void OnDisable()
		{
			_playerInput.Disable();
		}

		public Vector3 GetMoveDirection()
		{
			return _playerInput.Player.Move.ReadValue<Vector3>();
		}

		public Vector2 GetMousePosition()
		{
			return _playerInput.Player.MousePosition.ReadValue<Vector2>();
		}

		public bool IsAttacked()
		{
			return _playerInput.Player.Attack.triggered;
		}

		public bool IsInteract()
		{
			return _playerInput.Player.Interact.triggered;
		}
	}
}