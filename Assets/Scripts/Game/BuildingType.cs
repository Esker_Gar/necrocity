﻿namespace Game
{
    public enum BuildingType
    {
        Gate,
        TowerSpot,
        ArcherTower
    }
}