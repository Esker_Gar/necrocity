﻿using JetBrains.Annotations;
using Zenject;

namespace Game.Ecs
{
	[UsedImplicitly]
	public class EcsSystemsFactory
	{
		private readonly DiContainer _container;
		
		public EcsSystemsFactory(DiContainer container)
        {
            _container = container;
        }
		
		public T Create<T>()
			where T : class
		{
			return _container.Instantiate<T>();
		}
	}
}