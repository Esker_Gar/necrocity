﻿using Framework;
using JetBrains.Annotations;
using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;

namespace Game
{
	[UsedImplicitly]
	public class SyncAiViewPositionSystem : IEcsRunSystem
	{
		private EcsFilterInject<Inc<TransformComponent, PawnViewComponent>> _filter;
		private readonly EcsWorldInject _world;
		public void Run(IEcsSystems systems)
		{
			foreach (var i in _filter.Value)
			{
				ref var transformComponent = ref _filter.Pools.Inc1.Get(i);
				var pawnViewComponent = _filter.Pools.Inc2.Get(i);
				var positionComponent = _world.Value.GetComponent<PositionComponent>(pawnViewComponent.PawnModelEntity);

				if (_world.Value.HasComponent<EnemyComponent>(pawnViewComponent.PawnModelEntity))
				{
					transformComponent.Transform.position = positionComponent.Position;
				}
			}
		}
	}
}