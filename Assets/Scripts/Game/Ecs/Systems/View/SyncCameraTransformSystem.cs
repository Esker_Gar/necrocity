﻿using Framework;
using JetBrains.Annotations;
using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;
using UnityEngine;

namespace Game
{
	[UsedImplicitly]
	public class SyncCameraTransformSystem : IEcsRunSystem
	{
		private EcsFilterInject<Inc<CameraComponent>> _gameFilter;
		private readonly EcsFilterInject<Inc<PlayerComponent, PositionComponent>> _filter;
		private readonly EcsWorldInject _world;

		public void Run(IEcsSystems systems)
		{
			var playerId = _filter.Value.GetFirstId();
			var gameId = _gameFilter.Value.GetFirstId();
			ref var positionComponent = ref _filter.Pools.Inc2.Get(playerId);
			ref var cameraComponent = ref _gameFilter.Pools.Inc1.Get(gameId);
			var position = positionComponent.Position;

			cameraComponent.Transform.position = new Vector3(position.x + 3.88f, position.y + 34.69f, position.z - 35f);
		}
	}
}