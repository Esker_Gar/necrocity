﻿using Framework;
using JetBrains.Annotations;
using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;

namespace Game
{
	[UsedImplicitly]
	public class SyncViewRotationSystem : IEcsRunSystem
	{
		private EcsFilterInject<Inc<TransformComponent, PawnViewComponent>> _filter;

		private readonly EcsWorldInject _world;
		
		public void Run(IEcsSystems systems)
		{
			foreach (var i in _filter.Value)
			{
				ref var transformComponent = ref _filter.Pools.Inc1.Get(i);
				ref var pawnViewComponent = ref _filter.Pools.Inc2.Get(i);
				ref var rotationComponent = ref _world.Value.GetComponent<RotationComponent>(pawnViewComponent.PawnModelEntity);

				transformComponent.Transform.rotation = rotationComponent.Rotation;
			}
		}
	}
}