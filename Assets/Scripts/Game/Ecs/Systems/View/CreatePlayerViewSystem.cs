﻿using Framework;
using Game.UI;
using JetBrains.Annotations;
using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;
using UnityEngine;
using Zenject;

namespace Game.Ecs.Systems.View
{
	[UsedImplicitly]
	public class CreatePlayerViewSystem : IEcsInitSystem
	{
		[Inject] private readonly PlayerView _playerViewPrefab;
		[Inject] private readonly PlayerHealthUI _playerHealthUI;
		
		private EcsFilterInject<Inc<PlayerComponent, PositionComponent, MoveComponent, DeadComponent, HealthComponent, IdComponent>> _filter;
		private readonly EcsWorldInject _world;

		public void Init(IEcsSystems systems)
		{
			foreach (var i in _filter.Value)
			{
				var playerViewEntity = _world.Value.NewEntity();
				
				ref var positionComponent = ref _filter.Pools.Inc2.Get(i);
				ref var moveComponent = ref _filter.Pools.Inc3.Get(i);
				ref var deadComponent = ref _filter.Pools.Inc4.Get(i);
				ref var healthComponent = ref _filter.Pools.Inc5.Get(i);
				ref var idComponent = ref _filter.Pools.Inc6.Get(i);
				
				var playerView = Object.Instantiate(_playerViewPrefab, positionComponent.Position, Quaternion.identity);

				ref var transformComponent = ref _world.Value.AddComponent<TransformComponent>(playerViewEntity);
				ref var pawnViewComponent = ref _world.Value.AddComponent<PawnViewComponent>(playerViewEntity);
				ref var healthViewComponent = ref _world.Value.AddComponent<HealthViewComponent>(playerViewEntity);
				ref var viewIdComponent = ref _world.Value.AddComponent<IdComponent>(playerViewEntity);

				moveComponent.Movable = playerView;
				deadComponent.OnDead += () => _world.Value.AddComponent<DeadEventComponent>(playerViewEntity);
				healthComponent.OnChange += () => _world.Value.AddComponent<HealthChangeEventComponent>(playerViewEntity);
				pawnViewComponent.PawnModelEntity = i;
				transformComponent.Transform = playerView.transform;
				healthViewComponent.HealthUI = _playerHealthUI;
				viewIdComponent.Id = idComponent.Id;
				
				_playerHealthUI.SetMaxHealth(healthComponent.Health);
			}
		}
	}
}