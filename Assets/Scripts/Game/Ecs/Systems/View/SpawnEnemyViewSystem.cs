﻿using Framework;
using JetBrains.Annotations;
using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;
using UnityEngine;
using Zenject;

namespace Game.Ecs.Systems.View
{
	[UsedImplicitly]
	public class SpawnEnemyViewSystem : IEcsRunSystem
	{
		[Inject] private readonly EnemyView _enemyViewPrefab;
		
		private EcsFilterInject<Inc<SpawnEnemyEventComponent>> _filter;
		private readonly EcsWorldInject _world;

		public void Run(IEcsSystems systems)
		{
			foreach (var i in _filter.Value)
			{
				var enemyViewEntity = _world.Value.NewEntity();

				var spawnEnemyEventComponent = _filter.Pools.Inc1.Get(i);
				ref var aiMoverComponent = ref _world.Value.GetComponent<AiMoverComponent>(spawnEnemyEventComponent.EnemyEntity);
				var positionComponent = _world.Value.GetComponent<PositionComponent>(spawnEnemyEventComponent.EnemyEntity);
				ref var deadComponent = ref _world.Value.GetComponent<DeadComponent>(spawnEnemyEventComponent.EnemyEntity);
				ref var aiViewComponent = ref _world.Value.AddComponent<PawnViewComponent>(enemyViewEntity);
				ref var transformComponent = ref _world.Value.AddComponent<TransformComponent>(enemyViewEntity);

				var enemyView = Object.Instantiate(_enemyViewPrefab, positionComponent.Position, Quaternion.identity);
				aiViewComponent.PawnModelEntity = spawnEnemyEventComponent.EnemyEntity;
				aiMoverComponent.AiMover.SetSeeker(enemyView.Seeker);
				transformComponent.Transform = enemyView.transform;
				deadComponent.OnDead += () => _world.Value.AddComponent<DeadEventComponent>(enemyViewEntity);
			}
		}
	}
}