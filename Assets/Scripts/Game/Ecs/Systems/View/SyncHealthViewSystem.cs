﻿using Framework;
using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;

namespace Game.Ecs.Systems.View
{
    public class SyncHealthViewSystem : IEcsRunSystem
    {
        private EcsFilterInject<Inc<HealthChangeEventComponent, HealthViewComponent, IdComponent>> _filter;
        private readonly EcsWorldInject _world;

        public void Run(IEcsSystems systems)
        {
            foreach (var i in _filter.Value)
            {
                var idComponent = _filter.Pools.Inc3.Get(i);
                var healthViewComponent = _filter.Pools.Inc2.Get(i);
                var healthComponent = _world.Value.GetComponent<HealthComponent>(idComponent.Id);

                healthViewComponent.HealthUI.Set(healthComponent.Health);
            }
        }
    }
}