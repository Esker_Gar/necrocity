﻿using JetBrains.Annotations;
using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;
using UnityEngine;

namespace Game.Ecs.Systems.View
{
    [UsedImplicitly]
    public class DestroyViewSystem : IEcsRunSystem
    {
        private EcsFilterInject<Inc<DestroyEventComponent, TransformComponent>> _filter;
        private readonly EcsWorldInject _world;

        public void Run(IEcsSystems systems)
        {
            foreach (var i in _filter.Value)
            {
                ref var transformComponent = ref _filter.Pools.Inc2.Get(i);
				
                Object.Destroy(transformComponent.Transform.gameObject);
				
                _world.Value.DelEntity(i);
            }
        }
    }
}