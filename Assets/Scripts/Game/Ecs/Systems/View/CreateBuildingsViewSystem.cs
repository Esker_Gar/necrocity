﻿using Framework;
using JetBrains.Annotations;
using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;
using Zenject;

namespace Game
{
	[UsedImplicitly]
	public class CreateBuildingsViewSystem : IEcsInitSystem, IEcsRunSystem
	{
		[Inject] private readonly Configs _configs;
		[Inject] private readonly PrefabsInjectFactory _prefabsInjectFactory;

		private readonly EcsWorldInject _world;
		
		private EcsFilterInject<Inc<BuildingComponent, PositionComponent>> _filter;
		private EcsFilterInject<Inc<BuildEventComponent>> _newBuildingFilter;

		public void Init(IEcsSystems systems)
		{
			foreach (var i in _filter.Value)
			{
				CreateBuilding(i);
			}
		}

		public void Run(IEcsSystems systems)
		{
			foreach (var i in _newBuildingFilter.Value)
			{
				CreateBuilding(i);
			}
		}

		private void CreateBuilding(int entity)
		{
			var buildingComponent = _filter.Pools.Inc1.Get(entity);

			if (_configs.TryGetComponent(buildingComponent.BuildingId, out BuildingViewParameters buildingViewParameters))
			{
				var buildingViewEntity = _world.Value.NewEntity();
				
				ref var destroyComponent = ref _world.Value.GetComponent<DestroyComponent>(entity);
				var positionComponent = _filter.Pools.Inc2.Get(entity);
				var buildingView = _prefabsInjectFactory.Create(buildingViewParameters.Prefab);
				var transform = buildingView.transform;
				buildingView.Init(buildingComponent.BaseBuilding);
				transform.position = positionComponent.Position;
				transform.localScale = buildingViewParameters.Size;

				ref var transformComponent = ref _world.Value.AddComponent<TransformComponent>(buildingViewEntity);
				destroyComponent.OnDestroy += () => _world.Value.AddComponent<DestroyEventComponent>(buildingViewEntity);
					
				transformComponent.Transform = transform;
			}
		}
	}
}