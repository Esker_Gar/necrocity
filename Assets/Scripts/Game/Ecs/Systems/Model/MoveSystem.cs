﻿using JetBrains.Annotations;
using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;
using Zenject;

namespace Game
{
	[UsedImplicitly]
	public class MoveSystem : IEcsRunSystem
	{
		[Inject] private readonly GridController _gridController;
		
		private EcsFilterInject<Inc<MoveComponent, PositionComponent, CanMoveComponent>> _filter;

		public void Run(IEcsSystems systems)
		{
			foreach (var i in _filter.Value)
			{
				ref var moveComponent = ref _filter.Pools.Inc1.Get(i);
				ref var positionComponent = ref _filter.Pools.Inc2.Get(i);
				
				var position = moveComponent.Movable.Move(moveComponent.Direction * moveComponent.Speed);
				positionComponent.Position = position;
				
				var graphNode = _gridController.GetNode(position);
				_gridController.RemoveEntityOnNode(positionComponent.GraphNode, i);
				_gridController.AddEntityOnNode(graphNode, i);
				positionComponent.GraphNode = graphNode;
			}
		}
	}
}