﻿using Framework;
using JetBrains.Annotations;
using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;
using UnityEngine;

namespace Game
{
	[UsedImplicitly]
	public class TakeDamageSystem : IEcsRunSystem
	{
		private EcsFilterInject<Inc<TakeDamageEventComponent, HealthComponent>> _filter;
		private readonly EcsWorldInject _world;
			
		public void Run(IEcsSystems systems)
		{
			foreach (var i in _filter.Value)
			{
				ref var healthComponent = ref _filter.Pools.Inc2.Get(i);
				ref var takeDamageEventComponent = ref _filter.Pools.Inc1.Get(i);

				var previousHealth = healthComponent.Health;
				var currentHealth = previousHealth - takeDamageEventComponent.Damage;

				if (Mathf.Approximately(previousHealth, currentHealth) == false)
				{
					_world.Value.AddComponent<HealthChangeEventComponent>(i);
					
					healthComponent.Health = currentHealth;
					healthComponent.OnChange?.Invoke();
				}

				if (healthComponent.Health <= 0)
				{
					_world.Value.AddComponent<DeadEventComponent>(i);
				}
			}
		}
	}
}