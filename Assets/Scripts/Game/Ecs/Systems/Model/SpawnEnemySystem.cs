﻿using Framework;
using JetBrains.Annotations;
using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;
using UnityEngine;
using Zenject;

namespace Game
{
	[UsedImplicitly]
	public class SpawnEnemySystem : IEcsRunSystem
	{
		[Inject] private readonly Configs _configs;
		[Inject] private readonly GridController _gridController;

		private const float AllRound = 2 * Mathf.PI;

		private EcsFilterInject<Inc<EnemyWaveComponent>> _filter;
		private readonly EcsWorldInject _world;

		private float _enemySpawnInnerRadiusPow2;
		private float _enemySpawnOuterRadiusPow2;
		private int _currentEnemyAmount;

		public void Run(IEcsSystems systems)
		{
			var enemyConfig = _configs.GetConfig<EnemyConfigurations>();
			_enemySpawnInnerRadiusPow2 = enemyConfig.EnemySpawnInnerRadius * enemyConfig.EnemySpawnInnerRadius;
			_enemySpawnOuterRadiusPow2 = enemyConfig.EnemySpawnOuterRadius * enemyConfig.EnemySpawnOuterRadius;

			foreach (var i in _filter.Value)
			{
				ref var enemyWaveComponent = ref _filter.Pools.Inc1.Get(i);

				var enemies = enemyWaveComponent.PossibleEnemies;
				var randEnemy = Random.Range(0, enemies.Length);
				var enemyId = enemies[randEnemy];
				var enemyEntity = _world.Value.NewEntity();
						
				InitEnemy(enemyEntity, enemyId, enemyConfig);

				ref var spawnEnemyEventComponent = ref _world.Value.AddComponent<SpawnEnemyEventComponent>(i);
				spawnEnemyEventComponent.EnemyEntity = enemyEntity;

				enemyWaveComponent.CurrentEnemyAmount++;

				if (enemyWaveComponent.CurrentEnemyAmount == enemyWaveComponent.EnemyAmount)
				{
					_world.Value.DelEntity(i);
				}
			}
		}

		private void InitEnemy(int enemyEntity, int enemyId, EnemyConfigurations enemyConfig)
		{
			_world.Value.AddComponent<EnemyComponent>(enemyEntity);
			ref var moveComponent = ref _world.Value.AddComponent<MoveComponent>(enemyEntity);
			ref var behaviourTreeComponent = ref _world.Value.AddComponent<BehaviourTreeComponent>(enemyEntity);
			ref var positionComponent = ref _world.Value.AddComponent<PositionComponent>(enemyEntity);
			ref var aiMoverComponent = ref _world.Value.AddComponent<AiMoverComponent>(enemyEntity);
			ref var updatePathToTargetTimerComponent = ref _world.Value.AddComponent<UpdatePathToTargetTimerComponent>(enemyEntity);
			ref var healthComponent = ref _world.Value.AddComponent<HealthComponent>(enemyEntity);
			ref var attackComponent = ref _world.Value.AddComponent<AttackComponent>(enemyEntity);
			ref var idComponent = ref _world.Value.AddComponent<IdComponent>(enemyEntity);
			ref var teamComponent = ref _world.Value.AddComponent<TeamComponent>(enemyEntity);
			_world.Value.AddComponent<RotationComponent>(enemyEntity);
			_world.Value.AddComponent<DeadComponent>(enemyEntity);

			var randAngle = Random.Range(0, AllRound);
			var randRange = Random.Range(_enemySpawnInnerRadiusPow2, _enemySpawnOuterRadiusPow2);
			var radius = Mathf.Sqrt(randRange);
			var position = new Vector3(radius * Mathf.Cos(randAngle), -1.5f, radius * Mathf.Sin(randAngle));
			var graphNode = _gridController.GetNode(position);
			var enemyBt = new EnemyBT();
			enemyBt.Initialize(enemyEntity, _world.Value);

			positionComponent.Position = position;
			positionComponent.GraphNode = graphNode;
			
			behaviourTreeComponent.BehaviourTree = enemyBt;
			aiMoverComponent.AiMover = new AiMover(position, enemyConfig.NextWaypointDistance);
			updatePathToTargetTimerComponent.UpdatePathTime = enemyConfig.UpdatePathToTargetTime;

			if (_configs.TryGetComponent(enemyId, out UnitParameters unitParameters))
			{
				moveComponent.Speed = unitParameters.Speed;
				moveComponent.Movable = aiMoverComponent.AiMover;
				healthComponent.Health = unitParameters.Health;
			}

			if (_configs.TryGetComponent(enemyId, out AttackParameters attackParameters))
			{
				attackComponent.Damage = attackParameters.AttackDamage;
				attackComponent.AttackAngle = attackParameters.AttackAngle;
				attackComponent.AttackRange = attackParameters.AttackRange;
			}

			idComponent.Id = enemyId;
			teamComponent.TeamType = TeamType.Ai;

			_gridController.AddEntityOnNode(graphNode, enemyEntity);
		}
	}
}