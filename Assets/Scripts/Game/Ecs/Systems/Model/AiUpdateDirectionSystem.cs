﻿using JetBrains.Annotations;
using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;

namespace Game
{
	[UsedImplicitly]
	public class AiUpdateDirectionSystem : IEcsRunSystem
	{
		private EcsFilterInject<Inc<AiMoverComponent, MoveComponent>> _filter;
		
		public void Run(IEcsSystems systems)
		{
			foreach (var i in _filter.Value)
			{
				ref var aiMoverComponent = ref _filter.Pools.Inc1.Get(i);
				ref var moveComponent = ref  _filter.Pools.Inc2.Get(i);
				var aiMover = aiMoverComponent.AiMover;

				if (aiMover.IsCanUpdateDirection)
				{
					var direction = aiMover.GetDirection();
					moveComponent.Direction = direction;
				}
			}
		}
	}
}