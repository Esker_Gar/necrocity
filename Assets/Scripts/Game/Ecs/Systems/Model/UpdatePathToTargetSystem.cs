﻿using Framework;
using JetBrains.Annotations;
using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;

namespace Game
{
	[UsedImplicitly]
	public class UpdatePathToTargetSystem : IEcsRunSystem
	{
		private EcsFilterInject<Inc<AiMoverComponent, TargetComponent, UpdatePathToTargetEventComponent>> _filter;
		private readonly EcsWorldInject _world;

		public void Run(IEcsSystems systems)
		{
			foreach (var i in _filter.Value)
			{
				ref var aiMoverComponent = ref _filter.Pools.Inc1.Get(i);
				ref var targetComponent = ref _filter.Pools.Inc2.Get(i);
				ref var targetPositionComponent = ref _world.Value.GetComponent<PositionComponent>(targetComponent.TargetId);
				
				aiMoverComponent.AiMover.UpdatePath(targetPositionComponent.Position);
			}
		}
	}
}