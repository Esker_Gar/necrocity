﻿using Framework;
using JetBrains.Annotations;
using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;
using UnityEngine;
using Zenject;

namespace Game
{
	[UsedImplicitly]
	public class InputSystem : IEcsRunSystem, IEcsInitSystem
	{
		[Inject] private readonly PlayerInputManager _playerInputManager;
		
		private readonly EcsFilterInject<Inc<PlayerComponent, MoveComponent, RotationComponent, PositionComponent>> _filter;
		private readonly EcsWorldInject _world;

		private Camera _camera;
		private Plane _plane;

		public void Init(IEcsSystems systems)
		{
			_camera = Camera.main;
			_plane = new Plane(Vector3.up, Vector3.zero);
		}

		public void Run(IEcsSystems systems)
		{
			foreach (var i in _filter.Value)
			{
				ref var moveComponent = ref _filter.Pools.Inc2.Get(i);
				ref var rotationComponent = ref _filter.Pools.Inc3.Get(i);
				ref var positionComponent = ref _filter.Pools.Inc4.Get(i);
				
				var moveDirection = _playerInputManager.GetMoveDirection();
				var ray = _camera.ScreenPointToRay(_playerInputManager.GetMousePosition());

				if (_plane.Raycast(ray, out var enter))
				{
					var hitPoint = ray.GetPoint(enter);
					var playerPositionOnPlane = _plane.ClosestPointOnPlane(positionComponent.Position);
					var rotation = Quaternion.LookRotation(hitPoint-playerPositionOnPlane);
					
					rotationComponent.Rotation = rotation;
				}
				
				moveComponent.Direction = moveDirection;

				if (_playerInputManager.IsAttacked())
				{
					ref var attackEventComponent = ref _world.Value.AddComponent<AttackEventComponent>(i);
					attackEventComponent.AttackType = AttackType.Cone;
				}

				if (_playerInputManager.IsInteract())
				{
					_world.Value.AddComponent<InteractEventComponent>(i);
				}
			}
		}

	}
}