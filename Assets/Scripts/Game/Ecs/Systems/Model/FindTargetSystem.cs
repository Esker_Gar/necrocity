﻿using Framework;
using JetBrains.Annotations;
using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;

namespace Game
{
	[UsedImplicitly]
	public class FindTargetSystem : IEcsRunSystem
	{
		private EcsFilterInject<Inc<CanBeTargetComponent>> _canBeTargetFilter;
		private EcsFilterInject<Inc<FindTargetEventComponent>> _filter;
		private readonly EcsWorldInject _world;
		
		public void Run(IEcsSystems systems)
		{
			foreach (var i in _filter.Value)
			{
				ref var targetComponent = ref _world.Value.AddComponent<TargetComponent>(i);
				//TODO : Как появится больше нужно будет выбирать ближайшего
				targetComponent.TargetId = _canBeTargetFilter.Value.GetRawEntities()[0];
			}
		}
	}
}