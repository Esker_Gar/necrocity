﻿using JetBrains.Annotations;
using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;

namespace Game
{
	[UsedImplicitly]
	public class AiRotateSystem : IEcsRunSystem
	{
		private EcsFilterInject<Inc<RotationComponent, AiMoverComponent, PositionComponent, UpdatePathToTargetEventComponent>> _filter;

		public void Run(IEcsSystems systems)
		{
			foreach (var i in _filter.Value)
			{
				ref var rotationComponent = ref _filter.Pools.Inc1.Get(i);
				ref var aiMoverComponent = ref _filter.Pools.Inc2.Get(i);
				ref var positionComponent = ref _filter.Pools.Inc3.Get(i);

				var rotation = aiMoverComponent.AiMover.GetRotationToCurrentNode(positionComponent.Position);
				rotationComponent.Rotation = rotation;
			}
		}
	}
}