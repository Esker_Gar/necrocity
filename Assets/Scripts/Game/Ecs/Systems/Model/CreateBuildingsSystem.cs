﻿using Framework;
using JetBrains.Annotations;
using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;
using Zenject;

namespace Game
{
	[UsedImplicitly]
	public class CreateBuildingsSystem : IEcsInitSystem, IEcsRunSystem
	{
		[Inject] private readonly GridController _gridController;
		[Inject] private readonly Configs _configs;
		[Inject] private readonly BuildingFactory _buildingFactory;
		
		private readonly EcsWorldInject _world;
		
		private EcsFilterInject<Inc<BuildEventComponent>> _filter;

		public void Init(IEcsSystems systems)
		{
			foreach (BuildingParameters buildingParameters in _configs.GetAllComponents<BuildingParameters>())
			{
				CreateBuilding(buildingParameters);
			}
		}

		public void Run(IEcsSystems systems)
		{
			foreach (var i in _filter.Value)
			{
				var buildEventComponent = _filter.Pools.Inc1.Get(i);

				if (_configs.TryGetComponent(buildEventComponent.BuildingId, out BuildingParameters buildingParameters))
				{
					var newBuildingEntityId = CreateBuilding(buildingParameters);
					
					_world.Value.AddComponent<BuildEventComponent>(newBuildingEntityId);
				}

				_world.Value.DelComponent<BuildEventComponent>(i);
			}
		}

		private int CreateBuilding(BuildingParameters buildingParameters)
		{
			BaseBuilding building = _buildingFactory.GetBuilding(buildingParameters.BuildingType);
			
			building.Init(buildingParameters.Id, _world.Value, buildingParameters);

			return building.EntityId;
		}
	}
}