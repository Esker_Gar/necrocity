﻿using JetBrains.Annotations;
using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;
using Zenject;

namespace Game
{
	[UsedImplicitly]
	public class DeadSystem : IEcsRunSystem
	{
		[Inject] private GridController _gridController;
		
		private EcsFilterInject<Inc<DeadEventComponent, DeadComponent, PositionComponent>> _filter;
		private readonly EcsWorldInject _world;

		public void Run(IEcsSystems systems)
		{
			foreach (var i in _filter.Value)
			{
				ref var deadComponent = ref _filter.Pools.Inc2.Get(i);
				ref var positionComponent = ref _filter.Pools.Inc3.Get(i);
				
				_gridController.RemoveEntityOnNode(positionComponent.GraphNode, i);
				
				deadComponent.OnDead?.Invoke();
				deadComponent.OnDead = null;
				
				_world.Value.DelEntity(i);
			}
		}
	}
}