﻿using JetBrains.Annotations;
using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;

namespace Game.Ecs.Systems
{
	[UsedImplicitly]
	public class BehaviourTreeTickSystem : IEcsRunSystem
	{
		private EcsFilterInject<Inc<BehaviourTreeComponent>> _filter;

		public void Run(IEcsSystems systems)
		{
			foreach (var i in _filter.Value)
			{
				ref var behaviourTreeComponent = ref _filter.Pools.Inc1.Get(i);

				behaviourTreeComponent.BehaviourTree.Evaluate();
			}
		}
	}
}