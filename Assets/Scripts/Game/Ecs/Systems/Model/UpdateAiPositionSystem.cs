﻿using JetBrains.Annotations;
using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;

namespace Game
{
	[UsedImplicitly]
	public class UpdateAiPositionSystem : IEcsRunSystem
	{
		private EcsFilterInject<Inc<AiMoverComponent, PositionComponent>> _filter;

		public void Run(IEcsSystems systems)
		{
			foreach (var i in _filter.Value)
			{
				ref var aiMoverComponent = ref _filter.Pools.Inc1.Get(i);
				ref var positionComponent = ref  _filter.Pools.Inc2.Get(i);
				var aiMover = aiMoverComponent.AiMover;
				
				aiMover.UpdatePosition(positionComponent.Position);
			}
		}
	}
}