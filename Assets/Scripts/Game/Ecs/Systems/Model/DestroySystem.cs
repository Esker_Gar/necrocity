﻿using JetBrains.Annotations;
using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;

namespace Game.Ecs.Systems.Model
{
    [UsedImplicitly]
    public class DestroySystem : IEcsRunSystem
    {
        private EcsFilterInject<Inc<DestroyEventComponent, DestroyComponent>> _filter;
        private readonly EcsWorldInject _world;

        public void Run(IEcsSystems systems)
        {
            foreach (var i in _filter.Value)
            {
                ref var destroyComponent = ref _filter.Pools.Inc2.Get(i);
				
                destroyComponent.OnDestroy?.Invoke();
                destroyComponent.OnDestroy = null;
				
                _world.Value.DelEntity(i);
            }
        }
    }
}