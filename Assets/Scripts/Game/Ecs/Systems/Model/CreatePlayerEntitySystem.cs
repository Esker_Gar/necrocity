﻿using Framework;
using JetBrains.Annotations;
using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;
using UnityEngine;
using Zenject;

namespace Game
{
	[UsedImplicitly]
	public class CreatePlayerEntitySystem : IEcsInitSystem
	{
		[Inject] private readonly Configs _configs;
		[Inject] private readonly GridController _gridController;
		
		private readonly EcsWorldInject _world;

		public void Init(IEcsSystems systems)
		{
			var playerEntity = _world.Value.NewEntity();
			_world.Value.AddComponent<PlayerComponent>(playerEntity);
			_world.Value.AddComponent<RotationComponent>(playerEntity);
			ref var moveComponent = ref _world.Value.AddComponent<MoveComponent>(playerEntity);
			ref var positionComponent = ref _world.Value.AddComponent<PositionComponent>(playerEntity);
			ref var attackComponent = ref _world.Value.AddComponent<AttackComponent>(playerEntity);
			ref var healthComponent = ref _world.Value.AddComponent<HealthComponent>(playerEntity);
			ref var idComponent = ref _world.Value.AddComponent<IdComponent>(playerEntity);
			ref var teamComponent = ref _world.Value.AddComponent<TeamComponent>(playerEntity);
			_world.Value.AddComponent<CanBeTargetComponent>(playerEntity);
			_world.Value.AddComponent<CanMoveComponent>(playerEntity);
			_world.Value.AddComponent<DeadComponent>(playerEntity);

			var gameConfigs = _configs.GetConfig<GameConfigurations>();

			if (_configs.TryGetComponent(gameConfigs.PlayerId, out UnitParameters parameters))
			{
				moveComponent.Speed = parameters.Speed;
				healthComponent.Health = parameters.Health;
			}

			if (_configs.TryGetComponent(gameConfigs.PlayerId, out AttackParameters attackParameters))
			{
				attackComponent.Damage = attackParameters.AttackDamage;
				attackComponent.AttackAngle = attackParameters.AttackAngle;
				attackComponent.AttackRange = attackParameters.AttackRange;
			}
			
			var graphNode = _gridController.GetNode(Vector3.zero);
			_gridController.AddEntityOnNode(graphNode, playerEntity);

			positionComponent.Position = new Vector3(0, -1.5f, 0);
			positionComponent.GraphNode = graphNode;
			idComponent.Id = gameConfigs.PlayerId;
			teamComponent.TeamType = TeamType.Player;
		}
	}
}