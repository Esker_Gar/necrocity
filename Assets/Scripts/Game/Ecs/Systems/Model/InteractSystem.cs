﻿using Framework;
using JetBrains.Annotations;
using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;

namespace Game
{
	[UsedImplicitly]
	public class InteractSystem : IEcsRunSystem
	{
		private EcsFilterInject<Inc<InteractComponent, CanInteractComponent>> _interactFilter;
		private EcsFilterInject<Inc<PlayerComponent, InteractEventComponent>> _playerFilter;

		public void Run(IEcsSystems systems)
		{
			if (_interactFilter.Value.GetEntitiesCount() <= 0 ||
			    _playerFilter.Value.GetEntitiesCount() <= 0)
			{
				return;
			}

			var interactEntity = _interactFilter.Value.GetFirstId();
			var interactComponent = _interactFilter.Pools.Inc1.Get(interactEntity);
			
			interactComponent.Interactable.Interact();
		}
	}
}