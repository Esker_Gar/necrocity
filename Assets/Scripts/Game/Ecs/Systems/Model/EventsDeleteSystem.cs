﻿using Framework;
using JetBrains.Annotations;
using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;

namespace Game
{
	[UsedImplicitly]
	public class EventsDeleteSystem : IEcsRunSystem
	{
		private readonly EcsWorldInject _world;
		private EcsFilterInject<Inc<SpawnEnemyEventComponent>> _spawnFilter;
		private EcsFilterInject<Inc<UpdatePathToTargetEventComponent>> _updatePathFilter;
		private EcsFilterInject<Inc<FindTargetEventComponent>> _findTargetFilter;
		private EcsFilterInject<Inc<AttackEventComponent>> _attackTargetFilter;
		private EcsFilterInject<Inc<TakeDamageEventComponent>> _takeDamageTargetFilter;
		private EcsFilterInject<Inc<DeadEventComponent>> _deadFilter;
		private EcsFilterInject<Inc<InteractEventComponent>> _interactFilter;
		private EcsFilterInject<Inc<GateInteractEventComponent>> _gateInteractFilter;
		private EcsFilterInject<Inc<BuildEventComponent>> _buildFilter;
		private EcsFilterInject<Inc<DestroyEventComponent>> _destroyFilter;
		private EcsFilterInject<Inc<HealthChangeEventComponent>> _healthChangeFilter;

		public void Run(IEcsSystems systems)
		{
			_world.Value.DelEvent(_spawnFilter);
			_world.Value.DelEvent(_updatePathFilter);
			_world.Value.DelEvent(_findTargetFilter);
			_world.Value.DelEvent(_attackTargetFilter);
			_world.Value.DelEvent(_takeDamageTargetFilter);
			_world.Value.DelEvent(_deadFilter);
			_world.Value.DelEvent(_interactFilter);
			_world.Value.DelEvent(_gateInteractFilter);
			_world.Value.DelEvent(_buildFilter);
			_world.Value.DelEvent(_destroyFilter);
			_world.Value.DelEvent(_healthChangeFilter);
		}
	}
}