﻿using Framework;
using JetBrains.Annotations;
using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;
using Pathfinding;
using Zenject;

namespace Game.Ecs.Systems.Model
{
    [UsedImplicitly]
    public class GateInteractSystem : IEcsRunSystem
    {
        [Inject] private GridController _gridController;
        
        private EcsFilterInject<Inc<PositionComponent>> _positionFilter;
        private EcsFilterInject<Inc<GateInteractEventComponent, BuildingComponent>> _filter;
        private readonly EcsWorldInject _world;
		
        public void Run(IEcsSystems systems)
        {
            if (_filter.Value.GetEntitiesCount() <= 0)
            {
                return;
            }

            var interactEntity = _filter.Value.GetFirstId();
            var gateInteractEventComponent = _filter.Pools.Inc1.Get(interactEntity);
            var gateBuilding = (GatesBuilding)_filter.Pools.Inc2.Get(interactEntity).BaseBuilding;
            
            foreach (var i in _positionFilter.Value)
            {
                var positionComponent = _positionFilter.Pools.Inc1.Get(i);

                if (positionComponent.GraphNode == null)
                {
                    continue;
                }
                
                foreach (GraphNode graphNode in _gridController.PassabilityModifiers[gateInteractEventComponent.PassabilityModifier])
                {
                    if (graphNode.NodeIndex == positionComponent.GraphNode.NodeIndex)
                    {
                        return;
                    }
                }
            }

            gateBuilding.IsOpen = !gateBuilding.IsOpen;
            
            _gridController.SetNodesWalkable(gateInteractEventComponent.PassabilityModifier, gateBuilding.IsOpen);
        }
    }
}