﻿using Framework;
using JetBrains.Annotations;
using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;

namespace Game
{
	[UsedImplicitly]
	public class CheckInteractDistanceSystem : IEcsRunSystem
	{
		private EcsFilterInject<Inc<InteractComponent, PositionComponent>> _filter;
		private EcsFilterInject<Inc<PlayerComponent, PositionComponent>> _playerFilter;
		private readonly EcsWorldInject _world;

		public void Run(IEcsSystems systems)
		{
			var playerEntity = _playerFilter.Value.GetFirstId();
			var positionComponent = _playerFilter.Pools.Inc2.Get(playerEntity);

			foreach (var i in _filter.Value)
			{
				var interactComponent = _filter.Pools.Inc1.Get(i);
				var interactPositionComponent = _filter.Pools.Inc2.Get(i);

				var distancePow2 = (positionComponent.Position - interactPositionComponent.Position).sqrMagnitude;
				var hasCanInteractComponent = _world.Value.HasComponent<CanInteractComponent>(i);

				if (distancePow2 <= interactComponent.InteractRadiusPow2)
				{
					if (hasCanInteractComponent == false)
					{
						_world.Value.AddComponent<CanInteractComponent>(i);
					}
				}
				else
				{
					_world.Value.DelComponent<CanInteractComponent>(i);
				}
			}
		}
	}
}