﻿using System.Linq;
using Framework;
using JetBrains.Annotations;
using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;
using UnityEngine;
using Zenject;

namespace Game.Ecs.Systems
{
	[UsedImplicitly]
	public class CheckAttackSystem : IEcsRunSystem
	{
		[Inject] private AttackTypesKeeper _attackTypesKeeper;
		[Inject] private Configs _configs;
		
		private EcsFilterInject<Inc<AttackComponent, AttackEventComponent, IdComponent>, Exc<AttackCooldownComponent>> _filter;
		private readonly EcsWorldInject _world;
		
		public void Run(IEcsSystems systems)
		{
			foreach (var i in _filter.Value)
			{
				ref var attackEventComponent = ref _filter.Pools.Inc2.Get(i);
				ref var attackComponent = ref _filter.Pools.Inc1.Get(i);
				ref var idComponent = ref _filter.Pools.Inc3.Get(i);
				var attackType = _attackTypesKeeper.AttackTypes[attackEventComponent.AttackType];
				var targets = attackType.GetTargets(i, _world.Value).ToList();
				var maxTargets = targets.Count;

				if (targets.Count <= 0)
				{
					continue;
				}

				if (attackComponent.MaxTargets > 0 && attackComponent.MaxTargets <= targets.Count)
				{
					maxTargets = attackComponent.MaxTargets;
				}

				for (var index = 0; index < maxTargets; index++)
				{
					var targetId = targets[index];
					ref var takeDamageEventComponent = ref _world.Value.ResolveComponent<TakeDamageEventComponent>(targetId);
					takeDamageEventComponent.Damage += attackComponent.Damage;
				}

				if (_configs.TryGetComponent(idComponent.Id, out AttackParameters attackParameters))
				{
					ref var attackCooldownComponent = ref _world.Value.AddComponent<AttackCooldownComponent>(i);
					attackCooldownComponent.RemainingTime = attackParameters.Cooldown;
				}
			}
		}
	}
}