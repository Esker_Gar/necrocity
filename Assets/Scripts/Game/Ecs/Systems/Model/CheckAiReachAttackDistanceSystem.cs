﻿using Framework;
using JetBrains.Annotations;
using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;

namespace Game.Ecs.Systems
{
    [UsedImplicitly]
    public class CheckAiReachAttackDistanceSystem : IEcsRunSystem
    {
        private EcsFilterInject<Inc<PositionComponent, TargetComponent, AttackComponent, AiMoverComponent>> _filter;
        private readonly EcsWorldInject _world;
        
        public void Run(IEcsSystems systems)
        {
            foreach (var i in _filter.Value)
            {
                ref var attackComponent = ref _filter.Pools.Inc3.Get(i);
                ref var positionComponent = ref _filter.Pools.Inc1.Get(i);
                ref var targetComponent = ref _filter.Pools.Inc2.Get(i);
                ref var aiMoverComponent = ref _filter.Pools.Inc4.Get(i);
                var targetPositionComponent = _world.Value.GetComponent<PositionComponent>(targetComponent.TargetId);

                var distancePow2 = (positionComponent.Position - targetPositionComponent.Position).sqrMagnitude;
                
                aiMoverComponent.AiMover.SetReachTarget(distancePow2 <= attackComponent.AttackRange * attackComponent.AttackRange);
            }
        }
    }
}