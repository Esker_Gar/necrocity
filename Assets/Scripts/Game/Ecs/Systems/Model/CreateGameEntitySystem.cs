﻿using Framework;
using JetBrains.Annotations;
using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;
using UnityEngine;
using Zenject;

namespace Game
{
	[UsedImplicitly]
	public class CreateGameEntitySystem : IEcsInitSystem
	{
		[Inject] private readonly EcsManager _ecsManager;
		
		private readonly EcsWorldInject _world;

		public void Init(IEcsSystems systems)
		{
			var gameEntity = _world.Value.NewEntity();
			ref var cameraComponent = ref _world.Value.AddComponent<CameraComponent>(gameEntity);
			_world.Value.AddComponent<GameTimeComponent>(gameEntity);
			_world.Value.AddComponent<CanCreateEnemyWave>(gameEntity);
			
			var camera = Camera.main;
			
			cameraComponent.Camera = camera;
			cameraComponent.Transform = camera!.transform;
			
			_ecsManager.GameEntityId = gameEntity;
		}
	}
}