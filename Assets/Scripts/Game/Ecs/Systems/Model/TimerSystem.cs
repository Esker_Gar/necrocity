﻿using Framework;
using JetBrains.Annotations;
using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;
using UnityEngine;
using Zenject;

namespace Game
{
	[UsedImplicitly]
	public class TimerSystem : IEcsRunSystem
	{
		[Inject] private readonly Configs _configs;
		
		private EcsFilterInject<Inc<GameTimeComponent>> _filter;
		private EcsFilterInject<Inc<UpdatePathToTargetTimerComponent>> _pathFilter;
		private EcsFilterInject<Inc<AttackCooldownComponent>> _attackCooldownFilter;
		private EcsFilterInject<Inc<EnemyWaveComponent>> _enemyWaveFilter;
		private readonly EcsWorldInject _world;

		public void Run(IEcsSystems systems)
		{
			foreach (var i in _filter.Value)
			{
				ref var gameTimeComponent = ref _filter.Pools.Inc1.Get(i);
				gameTimeComponent.GameTime += Time.deltaTime;
				var gameConfig = _configs.GetConfig<GameConfigurations>();
				var canCreateEnemyWave = _world.Value.HasComponent<CanCreateEnemyWave>(i);

				if (gameTimeComponent.GameTime >= gameConfig.StartWaveTime && 
				    canCreateEnemyWave &&
				    _configs.TryGetComponent(1, out EnemyWaveParameters enemyWaveParameters))
				{
					var enemyWave = _world.Value.NewEntity();
					ref var enemyWaveComponent = ref _world.Value.AddComponent<EnemyWaveComponent>(enemyWave);
					enemyWaveComponent.Id = 1;
					enemyWaveComponent.EnemyAmount = enemyWaveParameters.EnemyAmount;
					enemyWaveComponent.PossibleEnemies = enemyWaveParameters.PossibleEnemies;

					_world.Value.DelComponent<CanCreateEnemyWave>(i);
				}

				if (gameTimeComponent.GameTime >= gameConfig.GameDayInSeconds)
				{
					gameTimeComponent.GameTime = 0;
					_world.Value.AddComponent<CanCreateEnemyWave>(i);
				}
			}

			foreach (var i in _pathFilter.Value)
			{
				ref var updatePathTimeComponent = ref _pathFilter.Pools.Inc1.Get(i);
				updatePathTimeComponent.RemainingTime -= Time.deltaTime;

				if (updatePathTimeComponent.RemainingTime <= 0)
				{
					_world.Value.AddComponent<UpdatePathToTargetEventComponent>(i);
					updatePathTimeComponent.RemainingTime = updatePathTimeComponent.UpdatePathTime;
				}
			}
			
			foreach (var i in _attackCooldownFilter.Value)
			{
				ref var attackCooldownComponent = ref _attackCooldownFilter.Pools.Inc1.Get(i);
				attackCooldownComponent.RemainingTime -= Time.deltaTime;

				if (attackCooldownComponent.RemainingTime <= 0)
				{
					_world.Value.DelComponent<AttackCooldownComponent>(i);
				}
			}
		}
	}
}