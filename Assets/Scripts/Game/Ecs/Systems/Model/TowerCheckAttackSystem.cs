﻿using Framework;
using JetBrains.Annotations;
using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;

namespace Game.Ecs.Systems.Model
{
    [UsedImplicitly]
    public class TowerCheckAttackSystem : IEcsRunSystem
    {
        private EcsFilterInject<Inc<AttackComponent, BuildingComponent, IdComponent>, Exc<AttackCooldownComponent>> _filter;
        private readonly EcsWorldInject _world;

        public void Run(IEcsSystems systems)
        {
            foreach (var i in _filter.Value)
            {
                ref var idComponent = ref _filter.Pools.Inc3.Get(i);
                
                ref var attackEventComponent = ref _world.Value.AddComponent<AttackEventComponent>(idComponent.Id);
                attackEventComponent.AttackType = AttackType.Circle;
            }
        }
    }
}