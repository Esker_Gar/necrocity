﻿namespace Game
{
	public struct InteractComponent
	{
		public IInteractable Interactable;
		public float InteractRadiusPow2;
	}
}