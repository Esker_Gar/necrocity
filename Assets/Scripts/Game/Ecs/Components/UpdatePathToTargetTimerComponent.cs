﻿namespace Game
{
	public struct UpdatePathToTargetTimerComponent
	{
		public float RemainingTime;
		public float UpdatePathTime;
	}
}