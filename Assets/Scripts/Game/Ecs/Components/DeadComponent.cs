﻿using System;

namespace Game
{
	public struct DeadComponent
	{
		public Action OnDead;
	}
}