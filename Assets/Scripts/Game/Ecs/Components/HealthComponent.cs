﻿using System;

namespace Game
{
	public struct HealthComponent
	{
		public float Health;
		public Action OnChange;
	}
}