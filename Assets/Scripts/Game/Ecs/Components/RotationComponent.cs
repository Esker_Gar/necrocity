﻿using UnityEngine;

namespace Game
{
	public struct RotationComponent
	{
		public Quaternion Rotation;
	}
}