﻿namespace Game
{
	public enum TeamType
	{
		Player,
		Ai
	}
	
	public struct TeamComponent
	{
		public TeamType TeamType;
	}
}