﻿using UnityEngine;

namespace Game
{
	public struct CameraComponent
	{
		public Camera Camera;
		public Transform Transform;
	}
}