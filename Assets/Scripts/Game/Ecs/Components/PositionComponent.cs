﻿using Pathfinding;
using UnityEngine;

namespace Game
{
	public struct PositionComponent
	{
		public Vector3 Position;
		public GraphNode GraphNode;
	}
}