﻿using System;

namespace Game
{
    public struct DestroyComponent
    {
        public Action OnDestroy;
    }
}