﻿using UnityEngine;

namespace Game
{
	public struct MoveComponent
	{
		public float Speed;
		public Vector3 Direction;
		public IMovable Movable;
	}
}