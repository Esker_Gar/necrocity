﻿namespace Game
{
	public struct EnemyWaveComponent
	{
		public int Id;
		public int CurrentEnemyAmount;
		public int EnemyAmount;
		public int[] PossibleEnemies;
	}
}