﻿namespace Game
{
	public struct BuildingComponent
	{
		public int BuildingId;
		public BaseBuilding BaseBuilding;
	}
}