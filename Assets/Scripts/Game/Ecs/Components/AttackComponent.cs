﻿namespace Game
{
	public struct AttackComponent
	{
		public float Damage;
		public float AttackRange;
		public float AttackAngle;
		public int MaxTargets;
	}
}