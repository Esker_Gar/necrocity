﻿using Framework;
using Leopotam.EcsLite;

namespace Game
{
	public class CheckTargetNode : Node
	{
		public override NodeState Evaluate(int entityId, EcsWorld world)
		{
			var hasTargetComponent = world.HasComponent<TargetComponent>(entityId);

			return hasTargetComponent ? NodeState.Success : NodeState.Failure;
		}
	}
}