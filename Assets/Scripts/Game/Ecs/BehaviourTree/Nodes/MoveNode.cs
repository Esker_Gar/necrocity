﻿using Framework;
using Leopotam.EcsLite;

namespace Game
{
	public class MoveNode : Node
	{
		public override NodeState Evaluate(int entityId, EcsWorld world)
		{
			var hasCanMoveComponent = world.HasComponent<CanMoveComponent>(entityId);

			if (hasCanMoveComponent == false)
			{
				world.AddComponent<CanMoveComponent>(entityId);
			}

			return NodeState.Running;
		}
	}
}