﻿using Framework;
using Leopotam.EcsLite;

namespace Game
{
	public class CheckAttackCooldownNode : Node
	{
		public override NodeState Evaluate(int entityId, EcsWorld world)
		{
			var hasCooldown = world.HasComponent<AttackCooldownComponent>(entityId);

			return hasCooldown ? NodeState.Success : NodeState.Failure;
		}
	}
}