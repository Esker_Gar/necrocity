﻿using System.Collections.Generic;
using Leopotam.EcsLite;

namespace Game
{
	public enum NodeState
	{
		Success,
		Failure,
		Running
	}

	public abstract class Node
	{
		protected NodeState State;
		protected readonly HashSet<Node> Children = new();
		
		private Node _parent;

		public Node(params Node[] nodes)
		{
			foreach (var node in nodes)
			{
				AttachChild(node);
			}
		}

		public abstract NodeState Evaluate(int entityId, EcsWorld world);

		private void AttachChild(Node child)
		{
			if (Children.Contains(child) == false)
			{
				child._parent = this;
				Children.Add(child);
			}
		}
	}
}