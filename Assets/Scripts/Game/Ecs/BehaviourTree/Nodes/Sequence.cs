﻿using Leopotam.EcsLite;

namespace Game
{
	public class Sequence : Node
	{
		public Sequence(params Node[] nodes) : base(nodes){}
		
		public override NodeState Evaluate(int entityId,  EcsWorld world)
		{
			if (Children.Count > 0)
			{
				foreach (var node in Children)
				{
					var childState = node.Evaluate(entityId, world);

					if (childState is NodeState.Failure or NodeState.Running)
					{
						return childState;
					}
				}
			}
			
			return  NodeState.Success;
		}
	}
}