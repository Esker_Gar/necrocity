﻿using Framework;
using Leopotam.EcsLite;

namespace Game
{
	public class FindTargetNode : Node
	{
		//TODO : думаю тут нужно будет расширить логику и возвращать не только success
		public override NodeState Evaluate(int entityId,  EcsWorld world)
		{
			world.AddComponent<FindTargetEventComponent>(entityId);

			return NodeState.Success;
		}
	}
}