﻿using Leopotam.EcsLite;

namespace Game
{
	public class Selector : Node
	{
		public Selector(params Node[] nodes) : base(nodes){}

		public override NodeState Evaluate(int entityId,  EcsWorld world)
		{
			if (Children.Count > 0)
			{
				foreach (var node in Children)
				{
					var childState = node.Evaluate(entityId, world);

					if (childState is NodeState.Success or NodeState.Running)
					{
						return childState;
					}
				}
			}
			
			return NodeState.Failure;
		}
	}
}