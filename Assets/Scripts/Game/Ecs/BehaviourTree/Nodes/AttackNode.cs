﻿using Framework;
using Leopotam.EcsLite;

namespace Game
{
	public class AttackNode : Node
	{
		public override NodeState Evaluate(int entityId, EcsWorld world)
		{
			ref var attackEventComponent = ref world.AddComponent<AttackEventComponent>(entityId);
			attackEventComponent.AttackType = AttackType.Cone;

			return NodeState.Success;
		}
	}
}