﻿using Framework;
using Leopotam.EcsLite;

namespace Game
{
	public class CheckDistanceNode : Node
	{
		public override NodeState Evaluate(int entityId, EcsWorld world)
		{
			ref var aiMoverComponent = ref world.GetComponent<AiMoverComponent>(entityId);

			if (aiMoverComponent.AiMover.IsReachTarget)
			{
				world.DelComponent<CanMoveComponent>(entityId);
			}

			return aiMoverComponent.AiMover.IsReachTarget ? NodeState.Success : NodeState.Failure;
		}
	}
}