﻿using Leopotam.EcsLite;

namespace Game
{
	public abstract class BehaviourTree
	{
		private Node _root = null;
		private int _entityId;
		private EcsWorld _world;
		
		protected abstract Node SetupTree();

		public void Initialize(int entityId, EcsWorld world)
		{
			_entityId = entityId;
			_world = world;
			_root = SetupTree();
		}

		public void Evaluate()
		{
			_root?.Evaluate(_entityId, _world);
		}
	}
}