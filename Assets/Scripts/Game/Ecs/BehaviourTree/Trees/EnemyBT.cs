﻿namespace Game
{
	public class EnemyBT : BehaviourTree
	{
		protected override Node SetupTree()
		{
			var tree = new Sequence(
				new Selector(new CheckTargetNode(), new FindTargetNode()),
				new Sequence(
					new Selector(new CheckDistanceNode(), new MoveNode()), 
					new Selector(new CheckAttackCooldownNode(), new AttackNode())));

			return tree;
		}
	}
}