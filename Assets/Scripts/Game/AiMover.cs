﻿using Pathfinding;
using UnityEngine;

namespace Game
{
	public class AiMover : IMovable
	{
		public bool IsCanUpdateDirection => _isCanUpdateDirection;
		public bool IsReachTarget => _isReachTarget;

		private readonly float _nextWaypointDistancePow2;
		private Path _path;
		private int _currentWaypoint;
		private Seeker _seeker;
		private Vector3 _position;
		private bool _isCanUpdateDirection;
		private Vector3 _targetPosition;
		private bool _isReachTarget;

		public AiMover(Vector3 position, float nextWaypointDistance)
		{
			_position = position;
			_nextWaypointDistancePow2 = nextWaypointDistance * nextWaypointDistance;
		}
		
		public Vector3 Move(Vector3 velocity)
		{
			if (_path != null)
			{
				var distancePow2 = (_position - _targetPosition).sqrMagnitude;

				if (distancePow2 < _nextWaypointDistancePow2)
				{
					if (_currentWaypoint + 1 < _path.vectorPath.Count)
					{
						_currentWaypoint++;
						_isCanUpdateDirection = true;
						_targetPosition = new Vector3(_path.vectorPath[_currentWaypoint].x, -1.5f, _path.vectorPath[_currentWaypoint].z);
					}
				}
				else
				{
					_position += velocity * Time.fixedDeltaTime;
				}
			}

			return _position;
		}

		public void SetSeeker(Seeker seeker)
		{
			_seeker = seeker;
		}

		public void SetReachTarget(bool state)
		{
			_isReachTarget = state;
		}

		public void UpdatePosition(Vector3 position)
		{
			_position = position;
		}

		public Vector3 GetDirection()
		{
			_isCanUpdateDirection = false;
			
			return (_targetPosition - _position).normalized;
		}

		public Quaternion GetRotationToCurrentNode(Vector3 position)
		{
			return Quaternion.LookRotation(Vector3.forward, _targetPosition - position);
		}

		public void UpdatePath(Vector3 targetPosition)
		{
			if (_seeker != null)
			{
				_seeker.StartPath(_position, targetPosition, OnPathComplete);
			}
		}

		private void OnPathComplete(Path path)
		{
			if (path.error == false)
			{
				_path = path;
				_currentWaypoint = 0;
				_isCanUpdateDirection = true;
				_targetPosition = new Vector3(_path.vectorPath[^1].x, -1.5f , _path.vectorPath[^1].z);
			}
		}
	}
}