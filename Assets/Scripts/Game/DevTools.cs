﻿using Framework;
using Leopotam.EcsLite;
using TMPro;
using UnityEngine;
using Zenject;

namespace Game
{
	public class DevTools : MonoBehaviour
	{
		[SerializeField] private bool _showHeroAttackRange;
		[SerializeField] private bool _showBuildingsRange;
		[SerializeField] private TMP_Text _timeText;

		[Inject] private readonly Configs _configs;
		[Inject] private readonly EcsManager _ecsManager;

		private const float NumSegments = 16;
		private const float AngleStep = 360f / NumSegments;

		private void OnDrawGizmos()
		{
			if (Application.isPlaying == false)
			{
				return;
			}

			if (_showHeroAttackRange)
			{
				DrawHeroAttack();
			}

			if (_showBuildingsRange)
			{
				DrawBuildingsRadius();
			}
		}

		private void Update()
		{
			var filter = _ecsManager.World.Filter<GameTimeComponent>().End();
			var gameId = filter.GetFirstId();
			var gameTimeComponent = _ecsManager.World.GetComponent<GameTimeComponent>(gameId);
			_timeText.SetText(gameTimeComponent.GameTime.ToString("0.00"));
		}

		private void DrawHeroAttack()
		{
			Gizmos.color = Color.green;
			var filter = _ecsManager.World.Filter<PlayerComponent>().End();
			var playerId = filter.GetFirstId();
			var attackComponent = _ecsManager.World.GetComponent<AttackComponent>(playerId);
			var positionComponent = _ecsManager.World.GetComponent<PositionComponent>(playerId);
			var rotationComponent = _ecsManager.World.GetComponent<RotationComponent>(playerId);
			var baseDirection = Vector3.forward;
			var lookDirection2d = rotationComponent.Rotation * baseDirection;
			var angleAttack = attackComponent.AttackAngle;
			var baseRadius = attackComponent.AttackRange * Mathf.Tan(angleAttack);
			var position = positionComponent.Position;
			var baseCenter = position + lookDirection2d * attackComponent.AttackRange;

			for (int i = 0; i < NumSegments; i++)
			{
				var angle = i * AngleStep;
				var x = baseRadius * Mathf.Cos(angle * Mathf.Deg2Rad);
				var y = baseRadius * Mathf.Sin(angle * Mathf.Deg2Rad);

				var pointOnCircle = Quaternion.LookRotation(lookDirection2d) * new Vector3(x, y, 0);
				Gizmos.DrawLine(position, baseCenter + pointOnCircle);
			}
		}

		private void DrawBuildingsRadius()
		{
			Gizmos.color = Color.red;
			var filter = _ecsManager.World.Filter<InteractComponent>().End();

			foreach (var i in filter)
			{
				var idComponent = _ecsManager.World.GetComponent<IdComponent>(i);
				
				if (_configs.TryGetComponent(idComponent.Id, out BuildingParameters buildingParameters))
				{
					var positionComponent = _ecsManager.World.GetComponent<PositionComponent>(i);
					
					Gizmos.DrawWireSphere(positionComponent.Position, buildingParameters.InteractRadius);
				}
			}
		}
	}
}