﻿using JetBrains.Annotations;
using UnityEngine;
using Zenject;

namespace Game
{
    [UsedImplicitly]
    public class Windows : IWindows
    {
        private const string WindowsPath = "Windows";
        
        [Inject(Id = CanvasType.UI)] private Canvas _uiCanvas;
        [Inject] private readonly PrefabsInjectFactory _prefabsInjectFactory;
        
        private Window _currentWindow;
        
        public void Open<T>(IWindowArgs args)
        where T: Window
        {
            var prefab = Resources.Load<T>(WindowsPath + $"/{typeof(T).Name}");

            if (prefab == null)
            {
                Debug.LogError("Can't find window with name " + typeof(T).Name);
            }
            else
            {
                _currentWindow = _prefabsInjectFactory.Create(prefab);
                var rect = _currentWindow.GetComponent<RectTransform>();
                
                rect.SetParent(_uiCanvas.transform);
                rect.offsetMin = Vector2.zero;
                rect.offsetMax = Vector2.zero;
            
                _currentWindow.Init(args);
                _currentWindow.OnClosed += OnWindowClosed;  
            }
        }

        private void OnWindowClosed()
        {
            _currentWindow.OnClosed -= OnWindowClosed;
            _currentWindow = null;
        }
    }
}