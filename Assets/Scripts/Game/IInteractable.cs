﻿namespace Game
{
	public interface IInteractable
	{
		void Interact();
	}
}