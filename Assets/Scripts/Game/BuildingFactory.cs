﻿using Zenject;

namespace Game
{
    public class BuildingFactory
    {
        [Inject] private readonly DiContainer _container;
        
        public BaseBuilding GetBuilding(BuildingType buildingType)
        {
            return buildingType switch
            {
                BuildingType.Gate => (BaseBuilding)_container.Instantiate(typeof(GatesBuilding)),
                BuildingType.TowerSpot => (BaseBuilding)_container.Instantiate(typeof(TowerSpotBuilding)),
                BuildingType.ArcherTower => (BaseBuilding)_container.Instantiate(typeof(ArcherTowerBuilding)),
            };
        }
    }
}