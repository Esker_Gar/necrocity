﻿using System;

namespace Framework
{
	public static class ReflectionExtensions
	{
		public static bool IsGenericSubclassOfRecursive(this Type typeToCheck, Type baseType)
		{
			if (typeToCheck == null || typeToCheck == typeof(object))
			{
				return false;
			}
			else if (typeToCheck.IsGenericType && typeToCheck.GetGenericTypeDefinition() == baseType)
			{
				return true;
			}
			else
			{
				return IsGenericSubclassOfRecursive(typeToCheck.BaseType, baseType);
			}
		}
	}
}