﻿using UnityEngine;

namespace Framework
{
    public static class MonoBehaviourExtensions
    {
        public static void DestroyChildren(this Transform transform)
        {
            int childCount = transform.childCount;

            for (int i = 0; i < childCount; i++)
            {
                GameObject child = transform.GetChild(i).gameObject;
                
                Object.Destroy(child);
            }
        }

        public static void SetActive(this MonoBehaviour monoBehaviour, bool isActive)
        {
            monoBehaviour.gameObject.SetActive(isActive);
        }
    }
}