﻿using Leopotam.EcsLite;
using Leopotam.EcsLite.Di;

namespace Framework
{
	public static class EcsExtensions
	{
		public static ref T GetComponent<T>(this  EcsWorld world, int entityIndex)
		where T : struct
		{
			var pool = world.GetPool<T>();
			ref var component = ref pool.Get(entityIndex);

			return ref component;
		}

		public static ref T ResolveComponent<T>(this EcsWorld world, int entityIndex)
			where T : struct
		{
			if (HasComponent<T>(world, entityIndex))
			{
				var pool = world.GetPool<T>();

				return ref pool.Get(entityIndex);
			}
			else
			{
				return ref AddComponent<T>(world, entityIndex);
			}
		}

		public static ref T AddComponent<T>(this EcsWorld world, int entityIndex)
		where T : struct
		{
			var pool = world.GetPool<T>();
			ref var component = ref pool.Add(entityIndex);

			return ref component;
		}

		public static void DelComponent<T>(this EcsWorld world, int entityIndex)
		where T : struct
		{
			if (HasComponent<T>(world, entityIndex))
			{
				var pool = world.GetPool<T>();
				pool.Del(entityIndex);
			}
		}
		
		public static void DelEvent<T>(this EcsWorld world, EcsFilterInject<Inc<T>> filter) 
			where T : struct
		{
			foreach (var entity in filter.Value)
			{
				world.DelComponent<T>(entity);
			}
		}

		public static bool HasComponent<T>(this EcsWorld world, int entityIndex)
		where T : struct
		{
			var pool = world.GetPool<T>();
			return pool.Has(entityIndex);
		}

		public static int GetFirstId(this EcsFilter filter)
		{
			return filter.GetRawEntities()[0];
		}
	}
}